<!DOCTYPE html>
<html>

<head>
    <title>Vídeos</title>
    <meta name="google-site-verification" content="N9GeyT3yuAMGK6ht7-k5Sqh4TfGpOGlJexQ0XCi-BlY" />
    <meta name="description" content="Vídeos com dicas sobre cuidados com os gatos e gatos brincando." />
    <meta name="keywords" content="comportamento do gato persa e exótico, individualidade de cada gato, 
        gato persa tranquilo, temperamento do gato exótico, macho ou fêmea de gato persa e exótico, gatil,
        Gatil Hauser, castração de gato, benefícios da castração dos gatos" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>

    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Vídeos</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <h2 class="titulo-texto">Vídeos em nosso canal no YouTube</h2>

            <div class="events-top">
                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/IEUVUeu0TV4">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/IEUVUeu0TV4" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/IEUVUeu0TV4">Como adaptar um gato novo em casa?</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>03/05/2022</span>
                    <p> Como acostumar um novo gato com os gatos antigos da casa? Veja neste vídeo como se desenvolve o
                        processo de adaptação.</p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/LrrCuNbWazU">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/LrrCuNbWazU" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/LrrCuNbWazU">Gato arranhando o sofá?</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>20/10/2022</span>
                    <p> Veja 6 dicas para evitar que o gato arranhe seu sofá.</p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/MFOciYkRU2E">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/MFOciYkRU2E" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/MFOciYkRU2E">Como cuidar de um gato</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>08/12/2022</span>
                    <p> Vai adotar, ganhar um gato e não sabe como cuidar? Ração, vacina, vermífugo, areia e muito mais.
                    </p>
                </div>

                <div class="clearfix"> </div>
            </div>

            <div class="events-top">
                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/AQvEa7OmOAQ">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/AQvEa7OmOAQ" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/AQvEa7OmOAQ">Como deixar um gato feliz dentro de casa?</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>11/05/2022</span>
                    <p>Neste vídeo dou muitas dicas do que nós podemos fazer para que nossos gatos sejam muito felizes e
                        saudáveis vivendo dentro de casa. </p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/Oss6V0_6dCg">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/Oss6V0_6dCg" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/Oss6V0_6dCg">Como educar um gato com mau comportamento?</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>24/07/2017</span>
                    <p> Neste vídeo mostrarei como ensinar e impor limites desde que o gatinho é bem pequeno para que
                        ele não adquira manias erradas.</p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/RkC28jSsJ0M">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/RkC28jSsJ0M" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/RkC28jSsJ0M">Dicas para o gato não pular na pia, mesa...</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>13/07/2022</span>
                    <p>Neste vídeo darei dicas de como fazer para que o gato não suba na pia, na mesa ou em qualquer
                        outro lugar. </p>
                </div>

                <div class="clearfix"> </div>
            </div>

            <div class="events-top">
                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/r8legbYWb6k">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/r8legbYWb6k" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/r8legbYWb6k">13 erros com a caixinha de areia do gato</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>14/04/2022</span>
                    <p> Caixa de areia com com cheiro ruim, torrões de xixi desmontando, o gato não querer usar a areia?
                        Neste vídeo reuni 13 coisas erradas que fazemos na areia do gato.</p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/VNAPywovSs4">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/VNAPywovSs4" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/VNAPywovSs4">Posso usar borrifador com água para ensinar o gato?</a>
                    </h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>22/04/2022</span>
                    <p> Será que utilizar borrifador é um bom método para ensinar um gato? Pode traumatizar o gato?
                        Neste vídeo falo a opinião do famoso Jackson Galaxy sobre este assunto.</p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/IlwaO3QkrJM">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/IlwaO3QkrJM" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/IlwaO3QkrJM">Dicas de produtos e faxina</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>12/10/2022</span>
                    <p> Quem tem animais de estimação passa mais trabalho limpando a casa, mas existem vários truques
                        para facilitar a faxina do nosso dia a dia.</p>
                </div>

                <div class="clearfix"> </div>
            </div>

            <div class="events-top">
                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/T1Y8ebKV8CY">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/T1Y8ebKV8CY" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/T1Y8ebKV8CY">Primeiros socorros em cães e gatos</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>23/08/2022</span>
                    <p>Como ajudar um gato ou cachorro em casos de engasgamento, queimaduras, fraturas e muito mais
                        antes da chegada do veterinário. </p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/uvncYyoyGXM">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/H-YMzHMZquM" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/uvncYyoyGXM">Dúvidas em relação as rações secas e úmidas</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>21/06/2022</span>
                    <p>Vantagens, desvantagens e diferenças entre a ração seca e a ração úmida para gatos e cachorros.
                        Neste vídeo falo sobre características positivas e negativas de cada ração. </p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/bBVa5stEqCs">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/bBVa5stEqCs" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/bBVa5stEqCs">Como cuidar gatinho orfão e recém nascido</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>04/04/2017</span>
                    <p> Neste vídeo dou dicas de como cuidar e alimentar gatinhos recém nascidos que não tem a mãe.</p>
                </div>

                <div class="clearfix"> </div>
            </div>

            <div class="events-top">
                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/H5pIe9pIlmY">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/H5pIe9pIlmY" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/H5pIe9pIlmY">Castração de gatos: mitos e verdades</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>20/08/2020</span>
                    <p> Neste vídeo eu falo sobre os mitos e as verdades em relação a castração.</p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/2gXCpd8_J08">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/2gXCpd8_J08" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/2gXCpd8_J08">Gatos com vermes?</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>06/01/2021</span>
                    <p> Neste vídeo falo sobre verminoses em gatos, como pega, quais os sintomas,
                        como se tratar, prevenir e como escolher um bom vermífugo. </p>
                </div>

                <div class="col-sm-4 top-event">
                    <a href="https://youtu.be/r0r5bbLT8Gg">
                        <iframe width="640" height="360" src="https://www.youtube.com/embed/r0r5bbLT8Gg" frameborder="0"
                            allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                    <h4><a href="https://youtu.be/r0r5bbLT8Gg">Gato faz cocô em lugar errado ou fora da areia?</a></h4>
                    <span><i class="glyphicon glyphicon-calendar"></i>11/08/2020</span>
                    <p> Muito se fala sobre gatos que fazem xixi em lugar errado e que demarcam território, mas também
                        tem gatos
                        que fazem cocô fora da caixa de areia. Neste vídeo eu falo de alguns motivos. </p>
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</body>

</html>