<header>
    

<div class="top-nav">
        <span class="menu"><img src="images/menu.png" alt=""> </span>
        <ul>
            <li class="active"><a href="index.html">Home</a></li>
            <li><a href="about.html">About</a></li>
            <li><a href="gallery.html">Gallery</a></li>
            <li><a href="blog.html">Blog</a></li>
            <li><a href="codes.html">Short Codes</a></li>
            <li><a href="contact.html">Contact</a></li>
            <div class="clearfix"> </div>
        </ul>

        <!--script-->
        <script>
            $("span.menu").click(function () {
                $(".top-nav ul").slideToggle(500, function () {
                });
            });
        </script>
    </div>

</header>
