<!DOCTYPE html>
<html>

<head>
    <title>Contato com a criadora - Gatil Hauser</title>
    <meta name="description" content="Entre em contato com a criadora para tirar dúvidas." />
    <meta name="keywords" content="contato, gatil, Gatil Hauser" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>   
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Contato</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <h2 class="titulo-texto">Ambientes e instalações</h2>

                        <p>Dois Irmãos, Rio Grande do Sul</p>
                        <p>Proprietária: Juliana Hauser</p>
                        <p>Email:<a href="mailto:gatilhauser@gmail.com"> gatilhauser@gmail.com</a></p>

                        <h2 class="titulo-texto" style="padding-top: 35px;">O Gatil Hauser nas redes sociais</h2>

                        <p><a href="mailto:gatilhauser@gmail.com"><img class="escala_redes" src="images/redes/email.png"
                                    alt="Email" /></a>
                            <a href="https://instagram.com/gatilhauser/" target="_blank"><img class="escala_redes"
                                    src="images/redes/instagram.png" alt="Instagram" /></a>
                            <a href="https://www.facebook.com/gatilhauser" target="_blank"><img class="escala_redes"
                                    src="images/redes/facebook.png" alt="Facebook" /></a>
                            <a href="https://www.youtube.com/user/GatilHauser" target="_blank"><img class="escala_redes"
                                    src="images/redes/youtube.png" alt="YouTube" /></a>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>