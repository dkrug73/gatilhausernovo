<!DOCTYPE html>
<html>

<head>
    <title>Como reservar e valor do filhote de gato persa ou exótico - Gatil Hauser</title>
    <meta name="description" content="Como reservar e qual o valor do filhote de gato persa ou exótico. 
				No valor do filhote está incluso a castração, todas as vacinas, o pedigree e a cópia dos exames dos pais." />
    <meta name="keywords" content="valor do gato persa exótico, quanto custa um gato persa exótico, gatil, Gatil Hauser,
				custo da criação, preço filhote de gato persa exótico, reserva de gato" />                
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>

    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">

    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Reserva e valor do filhote</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <h2 class="titulo-texto">Valores</h2>

                        <p>O valor deve ser pago em dinheiro ou transferência bancária (não trabalhamos com cartão de
                            crédito).
                            O valor é R$3.600,00 (três mil e seiscentos reais), distribuídos da seguinte forma: </p>

                        <ul class="topico">
                            <li>30% do valor do gatinho no momento da reserva;</li>
                            <li>70% restantes dez dias antes da data da entrega do gatinho (agendada previamente). </li>
                        </ul>

                        <h5>Faz parte do valor do gatinho:</h5>

                        <ul>
                            <li class="topico-item"><i class="glyphicon glyphicon-ok" style="color: #986733;"></i>
                                a castração;</li>

                            <li class="topico-item"><i class="glyphicon glyphicon-ok" style="color: #986733;"></i>
                                todas as vacinas (2 quádruplas e 1 da raiva);</li>

                            <li class="topico-item"><i class="glyphicon glyphicon-ok" style="color: #986733;"></i>
                                pedigree do gatinho;</li>

                            <li class="topico-item"><i class="glyphicon glyphicon-ok" style="color: #986733;"></i>
                                cópia dos exames de PKD, Fiv e Felv dos pais do gatinho (em pendrive);</li>

                            <li class="topico-item"><i class="glyphicon glyphicon-ok" style="color: #986733;"></i>
                                exames de Fiv e Felv do filhote;</li>

                            <li class="topico-item"><i class="glyphicon glyphicon-ok" style="color: #986733;"></i>
                                contrato de compra e venda com direitos e deveres de ambas as partes;</li>

                            <li class="topico-item"><i class="glyphicon glyphicon-ok" style="color: #986733;"></i>
                                vermifugação até a data de entrega;</li>

                            <li class="topico-item"><i class="glyphicon glyphicon-ok" style="color: #986733;"></i>
                                Pendrive com manual de cuidados e fotos do gatinho.</li>
                        </ul>

                        <br>
                        <p>Nenhum gatinho para estimação é vendido sem ser castrado. <strong>Sem exceção!</strong> </p>

                        <br>

                        <h2 class="titulo-texto">Entrega do filhote</h2>

                        <p>O gatinho deverá ser retirado no Gatil Hauser, em Dois Irmãos/RS. </p>

                        <p>Infelizmente, a Latam que era a única empresa que enviava gatos braquicefálicos sozinhos (sem
                            o dono), terceirizou o serviço de carga e o valor ficou partir de R$ 1.000,00 (mil reais).
                        </p>

                        <p> Com isso, a taxa de envio foi reajustada para um valor muito caro, não compensando mais o
                            envio aéreo.</p>

                        <p>Custa mais barato a pessoa comprar o vôo da madrugada e vir buscar o filhote.
                            A taxa cobrada pela Latam para o gatinho voltar com a pessoa na cabine do avião é 200 reais
                            e pode ser em qualquer horário.</p>

                        <p>Outra alternativa, <strong>para quem reside no sul</strong>, é vir buscar de carro.
                            Neste caso, dependendo da distância, podemos levar até um determinado ponto e nos
                            encontramos no caminho.</p>

                        <br>

                        <h2 class="titulo-texto">Castração</h2>

                        <p>Obedecemos aos códigos de ética da criação. Portanto todos os nossos filhotes vendidos para
                            companhia são castrados antes de serem entregues ao novo proprietário. Essa é uma medida
                            adotada universalmente por criadores renomados visando preservar o bom desenvolvimento da
                            raça, além de que a castração é um dos preceitos básicos da posse responsável.</p>

                        <p>Os filhotes são efetivamente reservados após a assinatura do contrato e o pagamento do valor
                            da reserva. Não existe reserva apenas com promessa de compra. Você deverá entrar em contato
                            para fazer a reserva.</p>

                        <p>Em um primeiro momento, você pode achar o valor um pouco caro, mas é preciso ter consciência
                            de você está adquirindo um gato de um criador sério, com uma criação correta. Nosso Gatil é
                            registrado, livre de doenças (todo plantel foi testado para PKD, FIV e Felv), usamos somente
                            ração super premium e, principalmente, nossos gatos vivem em um ambiente higienizado e
                            saudável. Além disso, todos os filhotes são entregues castrados e vacinados. Não tem como
                            comparar o valor cobrado, tendo como base uma criação fundo de quintal, visto que, na grande
                            maioria dos casos, a ração utilizada é de baixa qualidade, os filhotes são entregues, muitas
                            vezes, sem vacina, não são castrados, não tem pedigree (procedência duvidosa ou mistura com
                            gatos sem raça definida), com doenças e fungos. E, ainda, entregam os filhotes precocemente
                            para se “livrar” dos gastos. Você pode até achar interessante receber o gato mais cedo, mas
                            para o filhote isto é cruel, pois ele é tirado do convívio materno muito cedo, prejudicando
                            seu desenvolvimento e, principalmente, sua socialização. </p>

                        <p>Criação de gatos de raça é uma coisa, comércio descontrolado de animais é bem outra! </p>

                        <p>E, além disso, um gatil correto pode ter no máximo uma cria a cada 8 meses de cada gata. Ao
                            contrário dos "criadores" fundo de quintal que tiram uma cria atrás da outra de cada fêmea,
                            pois visam somente o lucro, sem se preocupar com a saúde dos gatos.</p>

                        <p>Sabe aquele velho ditado de que o barato sai caro? Ele se aplica muito bem quando falamos dos
                            criadores fundo de quintal. O valor, que em um primeiro momento foi mais barato, em pouco
                            tempo acaba se multiplicando com gastos em tratamentos veterinários que o gatinho precisará
                            (remédios orais, tópicos, banhos medicinais e até internação), ou até mesmo com a sua morte
                            precoce, caso ele tenha alguma doença incurável, como FIV, FELV, que só são detectadas
                            através de exames nos pais (que geralmente só os melhores gatis fazem, porque isso gera
                            custos), ou com alguma infecção que o gatinho pegue enquanto ainda não tiver resistência
                            para combater as doenças.</p>

                        <p>Desculpa se o texto é meio grosseiro, mas tem gente que não tem nem noção dos custos e,
                            principalmente, da grande diferença que existe entre criadores e "criadores" e por isso
                            acham que os gatinhos custam caro.</p>

                        <p>Os meus filhotinhos são entregues com quatro meses e durante todo esse tempo eles tem o
                            melhor tratamento possível. </p>

                        <p>Para quem não tem muita ideia de preços, dê uma pesquisa nos valores da castração, das
                            vacinas, da ração super premium, consulta com veterinário, ultrassonografia, areia
                            higiênica, vitaminas, vermífugos, leite para filhotes (pet milk), água, luz, gasolina,
                            banhos, anuidade do clube, pedigrees dos filhotes, noites em claro, entre outras coisas.
                        </p>

                        <p>Isso tudo contando que corra tudo bem no parto, porque se a gata precisar fazer uma cesariana
                            são mais 500 reais em veterinário e mais dinheiro em remédios. </p>

                        <p>E sempre existe o risco de algum filhote nascer morto ou muito fraco e não resistir. Afinal,
                            estamos lidando com vidas. Portanto, boa parte do valor é apenas custos.</p>

                        <p>E não são somente os filhotinhos, não é mesmo? Existem os gatos adultos também, o ano todo,
                            todos os dias, mesmo quando não temos filhotinhos a venda.</p>

                        <p>Pense, com todos esses custos, como um filhote com essa qualidade poderia custar tão barato
                            como se vê nos "criadores" fundo de quintal? Impossível, não é mesmo?!</p>

                        <p>Acredito que com essa explicação ficou bastante óbvia a diferença na qualidade da criação e
                            também que criação de gatos não é tão simples como alguns imaginam. "Nem tudo são flores."
                        </p>

                        <br>

                        <h2 class="titulo-texto">Você está preparado para ter um gato?</h2>

                        <p>Ter um animal de estimação não é barato. Eles precisam ir ao veterinário, fazer vacinas,
                            tomar banho,
                            tomar vermífugo, comer ração de ótima qualidade, tosas, arranhadores, brinquedos e se
                            acontecer de
                            um dia ficarem doentes, muitas vezes o tratamento custa caro. Por isso é importante pensar
                            muito
                            bem antes de adquirir um bichinho. Isso implica em gastos. Também precisam de atenção e
                            cuidados
                            com a higiene. Precisam ser escovados com muita frequência, limpar os ouvidos, os olhos,
                            cortar
                            as unhas. Gatos miam, soltam pelos, correm pela casa, sobem nos móveis, na mesa... </p>

                        <p>Gatos podem viver até 20 anos. Você está disposto a cuidar dele durante todo esse tempo,
                            aconteça o que acontecer? Está disposto a mantê-lo com você, mesmo que mude de cidade,
                            case ou tenha filhos? Lembre-se que os animais têm sentimentos e sofrem com rejeição e
                            abandono.
                            Um gatinho não é uma mercadoria! É um ser vivo que tem necessidades fisiológicas e espera
                            carinho,
                            compreensão, companheirismo, interatividade e amor de quem os têm.</p>

                        <p>Um gatinho não é uma mercadoria! É um ser vivo que tem necessidades fisiológicas e espera
                            carinho,
                            compreensão, companheirismo, interatividade e amor de quem os têm. Em outras palavras,
                            manter um
                            animal de estimação, sendo ele de raça ou não, requer alguns investimentos, principalmente
                            em
                            relação à sua saúde.</p>

                        <p>Parece meio óbvio falar disso, mas é sempre bom lembrar: um gato não sabe falar ou se
                            expressar
                            quando está doente ou sentindo algo. Isto significa que haverá gastos com veterinário e
                            possíveis
                            tratamentos. Infelizmente existem pessoas que não se apegam, não amam os seus bichinhos e em
                            um
                            momento de dificuldade preferem abandoná-los do que cuidar deles.</p>

                        <p>É extremamente necessário refletir sobre o assunto antes da compra, realizar essa autoanálise
                            para
                            que mais tarde o gato não se torne mais um bichinho abandonado a própria sorte como vemos
                            tantos
                            pelas ruas. Se você realmente ama os animais, especialmente os gatos, tenha certeza que ele
                            irá
                            lhe fazer muito feliz, pois gatos são carinhosos, companheiros, calmos, independentes,
                            quase não dão trabalho.</p>

                        <p id="pkd"><a href="#" class="scroll">Clique aqui para retornar ao início.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>