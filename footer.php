<div class="footer-1"> </div>
<div class="container">
    <div class="footer_top">
        <div class="col-md-3 footer-head">
            <h4>Redes sociais</h4>
            <ul class="socials">
                <li><a href="https://www.facebook.com/gatilhauser" target="_blank"><i ></i></a></li>
                <li><a href="https://instagram.com/gatilhauser/" target="_blank"><i class="instagram"></i></a></li>
                <li><a href="https://www.youtube.com/user/GatilHauser" target="_blank"><i class="youtube"></i></a></li>
            </ul>
        </div>

        <div class="clearfix"> </div>
    </div>
    <div class="footer-bottom">
        <p> Especializado em gatos persas e exóticos. Campo Bom - Rio Grande do Sul.</p>
    </div>
</div>