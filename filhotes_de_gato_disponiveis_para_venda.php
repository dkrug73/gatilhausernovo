<!DOCTYPE html>
<html>

<head>
    <Jully>Filhotes disponíveis</Jully>
    <meta name="description" content="Filhotes de gato persa e exóticos disponíveis para venda. 
			Todos os gatinhos são vendidos castrados, com todas as vacinas, pedigree e exames de PKD, Fiv e Felv dos pais." />
    <meta name="keywords" content="filhote de gato persa, filhote de gato exótico, gatos a venda, 
        filhote de gato a venda, gatos de raçaa, gatil, Gatil Hauser" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>

    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">

    <script type="text/javascript" charset="utf-8">
    $(function() {
        $('.gallery-top a').Chocolat();
    });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Filhotes disponíveis</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <h2 class="titulo-texto">Filhotes disponíveis para venda</h2>
                        <p><strong>NENHUM FILHOTE DO GATIL HAUSER É ANUNCIADO EM SITES DE VENDAS (Mercado Livre, OLX
                                etc.). </strong></p>

                        <p>O contato se dá exclusivamente através dos nossos canais, como
                            <strong>FACEBOOK</strong>, <strong>INSTAGRAM</strong>, <strong>EMAIL</strong> ou
                            <strong>YOUTUBE</strong>.
                        </p>

                        <p>Para obter mais informações de como reservar seu gatinho acesse a página
                            <a href="reserva_e_valor_do_gato.php"><strong>RESERVA E VALOR</strong></a>.
                        </p>

                        <br>

                        <div class="col-md-4 gallery-top"style="width: 100%;">
                            <img src="images/disponiveis/nao_tem_gatinhos.jpg" alt="Persa" />
                        </div>

                         <!--
                        <h2 class="titulo-texto">Disponível para reserva</h2>

                        <p style="margin: auto;"><strong>Nascimento</strong>: 03 de março de 2023.</P>
                        <p style="margin: auto;"><strong>Entrega</strong>: disponível para entrega a partir de
                            <strong>08/07/2023</strong><strong></strong>.
                        </p>
                        <p style="margin: auto;">Todos os gatinhos são entregues já castrados, vacinados, com pedigree,
                            exames de Fiv e Felv e com cópia dos exames de PKD, Fiv e Felv dos pais.</p>

                       

                        <h2 class="titulo-texto">Disponível para venda</h2>

                        <p style="margin: auto;">A Jully nasceu em agosto de 2021</p>
                        <p style="margin: auto;">Ela foi castrada porque teve dificuldades no parto.
                            Ela é jovem e saudável.
                        </p>
                        <p style="margin: auto;">Apenas não deu certo para ser mamãe.</p>
                        <p style="margin: auto;">Está castrada, vacinada, vermifugada e tem exames negativos
                            de PKD, Fiv e Felv.
                        </p>
                        <p style="margin: auto;">Valor: <strong>R$ 1.500,00</strong></p>
                        <p style="margin: auto;">É obrigatório que a casa ou apartamento seja totalmente telado.</p>
                        -->

                    </div>
                </div>
            </div>
        </div>


        
           
                
              
                

                <!--
                <div class="grid">
                    <video controls poster="" style="max-width: 98.7%;padding-left: 15px;">
                        <source src="videos/persa.MP4" type="video/mp4">
                        <source src="videos/persa.webm" type="video/webm">
                        <source src="videos/persa.ogv" type="video/ogg">
                        <object>
                        <embed src="persa.mp4" type="application/x-shockwave-flash" 
                        allowfullscreen="false" allowscriptaccess="always">  		
                        </object>
                        Formato não suportado  
                    </video>
                </div>
                -->

                <!--
                <div style="padding-left: 15px;">
                    <a href="https://www.premierpet.com.br/" target="_blank"><img class="logo-premier"
                            src="images/premiere2.jpg" alt="Premier Pet" /></a>
                </div>
                -->
                
            </div>
        </div>

    </div>
</body>

</html>