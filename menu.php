<?php
$pagina = $_SERVER['REQUEST_URI'];
?>

<div class="container">
    <div class="logo">
        <a href="index.php"><img class="imagem-logo" src="images/logo1.png" alt="logo"></a>
    </div>
    <div class="head-right">
        <div class="top-nav">
            <span class="menu"><img src="images/menu.png" alt=""> </span>
            <ul>
                <li <?php ($pagina == "/index.php") ? print "class=active" : "" ?>>
                    <a href="index.php">Início</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Filhotes <i class="fa fa-caret-down"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="filhotes_de_gato_disponiveis_para_venda.php" style="min-width: 170px;"> Disponíveis </a>
                        <a href="filhotes_de_gato_vendidos.php" style="min-width: 170px;">Vendidos </a>
                    </div>
                </li>

                <li <?php ($pagina == "/reserva_e_valor_do_gato.php") ? print "class=active" : "" ?>>
                    <a href="reserva_e_valor_do_gato.php">Reserva e Valor</a>
                </li>

                <li <?php ($pagina == "/videos_de_gatos.php") ? print "class=active" : "" ?>>
                    <a href="videos_de_gatos.php">Vídeos</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Cuidados <i class="fa fa-caret-down"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="cuidados_antes_de_comprar_um_gato.php">Cuidados antes da compra</a>
                        <a href="fase_de_adaptacao_do_gato.php">Fase de adaptação</a>
                        <a href="como_cuidar_de_um_gato.php">Recomendações e produtos</a>
                        <a href="comportamento_do_gato_e_castracao.php">Comportamento e castração</a>
                        <a href="doencas_de_gato.php">Doenças</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Meu gatil <i class="fa fa-caret-down"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="meus_gatos_mamaes_e_papais.php">Mamães e Papais</a>
                        <a href="pedigrees_e_exames_dos_gatos.php">Pedigrees e exames</a>
                        <a href="o_gatil.php">Instalações</a>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Contato <i class="fa fa-caret-down"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="contato.php" style="min-width: 170px;">E-mail</a>
                        <a href="quem_sou.php" style="min-width: 170px;">Quem sou</a>
                    </div>
                </li>

            </ul>

            <!--script-->
            <script>
            $("span.menu").click(function() {
                $(".top-nav ul").slideToggle(500, function() {});
            });
            </script>
        </div>
    </div>
    <div class="clearfix"> </div>
</div>