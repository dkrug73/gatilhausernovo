<!DOCTYPE html>
<html>

<head>
    <title>Quem sou - Gatil Hauser</title>
    <meta name="description" content="Quem sou. Meu nome é Juliana, tive minha primeira gatinha SRD aos 7 anos de idade e de lá para cá sempre amei gatos. 
			Em 2011 comecei a criar gatos persas e posteriormente também exóticos." />
    <meta name="keywords" content="criadora de gatos persas e exóticos, gatil, Gatil Hauser" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Quem sou</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <p>Meu nome é <strong>Juliana Hauser</strong>, sou natural de Novo Hamburgo/RS, mas moro em Dois
                            Irmãos, também
                            no Rio Grande do Sul. Sou Turismóloga (Bacharel em Turismo) e especialista em Gerenciamento
                            de Vendas, porém, gatos sempre foram a paixão da minha vida. Aos 7 anos de idade (1984)
                            ganhei minha primeira gatinha SRD (sem raça definida = vira-lata) de uma vizinha. Era uma
                            gata toda preta, bem pequeninha. Por falta de criatividade, ela ganhou o nome de Mimo preta
                            (risos).</p>

                        <p>Na época não havia essa consciência da importância de castrar os gatos, então depois de um
                            tempo ela teve cria. Alguns gatos nós conseguimos doar e outros acabaram ficando lá em casa
                            e isso se repetiu outras vezes. Teve uma época que tínhamos quase 40 gatos. Eles viviam
                            soltos, iam em casa para dormir e para comer. Até a idade adulta sempre tivemos alguns gatos
                            SRD.</p>

                        <p>Mas foi no comecinho de 2005, já casada, que eu e meu marido compramos nossa primeira gata
                            persa, a Biluka. Ela tinha 2 meses, era muito pequeninha, mas muito linda. Ela estava a
                            venda em uma pecuária e por isso não recebia os cuidados necessários. Estava cheia de pulgas
                            e com otite. Preparamos todo enxoval dela (arranhador, ração, brinquedos, caminha, bacia de
                            areia...) e fomos buscá-la.</p>

                        <p>A Biluka foi castrada na primeira vez que entrou no cio porque ela miava muito e ninguém
                            conseguia dormir (risos) e nós não pensávamos em ter gatil naquela época, até mesmo porque a
                            Biluka é padrão Pet (com o nariz mais comprido, mais baixo). No começo de 2006 fomos morar
                            em Fortaleza/CE e em final de 2008 fomos morar em Joinville/SC e meados de 2011 voltamos
                            para o Rio Grande do Sul. A Biluka sempre nos acompanhou o tempo todo. Ela é uma gata
                            bastante viajada rss.</p>

                        <p>Mas voltando um pouco no tempo, em 2009, ainda em Joinville, tivemos vontade de ter mais uma
                            gata para fazer companhia e brincar com a Biluka, então compramos a Catita. No começo elas
                            se estranharam, a Biluka, que sempre foi acostumada sozinha, não queria a amizade da Catita.
                            Com o tempo a Biluka ficou mais calma, mas nunca ficou amiga da Catita como esperávamos.
                            Então, no começo de 2010 compramos mais um gato para brincar com a Catita, já que a Biluka
                            não é muito sociável rss. Dessa vez foi um machinho, o Iron, também persa. Ele e a Catita
                            ficaram amigos, brincavam bastante, até que a Catita entrou no cio, e o Iron acabou
                            namorando com ela (mesmo sem nós esperarmos por isso, pois ele tinha apenas 10 meses) e a
                            Catita ficou grávida.</p>

                        <p>Em dezembro de 2010 tivemos a cria da Catita. Nasceram 4 gatinhos. A Catita foi uma ótima
                            mãe, cuidava dos gatinhos o tempo todo. Foi uma felicidade ter essas bolinhas de pelos
                            brincando, correndo, sempre alegrando nosso lar. E foi após esse acontecimento que decidimos
                            ter um gatil especializado em gato persa, visto nossa paixão por gatos há tantos anos.
                            Porém, com essa decisão, foi necessário adquirir outros gatos. Gatos registrados, com
                            pedigree, para poder ter uma criação. O Iron e a Catita foram castrados porque foram
                            comprados como gatos de estimação e não tinha pedigree para Criação.</p>

                        <p>Em março de 2011 compramos um macho, o Baruk, e posterior a isso, já de volta ao RS,
                            compramos uma fêmea, a Gigi e meados de 2012 compramos a Meggy. Final de 2013 e começo de
                            2014 compramos a Havanna, a Ozzie e a Rock. Meados de 2015 compramos outro macho, desta vez
                            um exótico, o Freddy.</p>

                        <p>Durante o ano de 2011 nós pesquisamos e estudamos sobre o gato persa, os cuidados,
                            comportamento, alimentação, higiene, as doenças, cruzamentos, padrões etc. E para iniciar um
                            gatil com ótima qualidade, nossa primeira atitude foi realizar exames de PKD (Doença Renal
                            Policística), FIV (Síndrome de Imunodeficiência Felina) e FELV (Leucemia Felina) em todos os
                            nossos gatos. Pais negativos geram filhotes negativos.</p>

                        <p>No 2º semestre de 2011 registramos o gatil e nos tornamos filiados do Feline Club, de SP e
                            posteriormente no Clube UNIGAT, do RS.</p>

                        <p>Nossos gatos são, e sempre foram alimentados com ração Super Premium, tem todas as vacinas em
                            dia, são vermirfugados, não tem acesso a rua e nem contato com outros animais. Nossa casa
                            tem grades em todas as aberturas e o espaço externo destinado aos gatos para banho de sol é
                            completamente fechado com muros altos.</p>

                        <p>Os filhotes não são desmamados precocemente e são entregues já castrados, por isso só
                            entregamos os gatinhos com 4 meses de idade. Nesta idade já estão vacinados, vermifugados,
                            já sabem usar a areia higiênica e estão sociabilizados.</p>

                        <p style="margin-top: 30px;">Juliana Hauser</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>