<!DOCTYPE html>
<html>

<head>
    <title>Quem sou - Gatil Hauser</title>
    <meta name="description" content="Quem sou. Meu nome é Juliana, tive minha primeira gatinha SRD aos 7 anos de idade e de lá para cá sempre amei gatos. 
			Em 2011 comecei a criar gatos persas e posteriormente também exóticos." />
    <meta name="keywords" content="criadora de gatos persas e exóticos, gatil, Gatil Hauser" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script> 
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="gallery" style="padding-top: 35px;">
        <div class="container">
            <embed src="https://gatilhauser.com.br/contrato.pdf" width="100%" height="860px">
        </div>
    </div>
</body>

</html>