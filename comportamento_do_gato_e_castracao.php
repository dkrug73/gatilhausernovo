<!DOCTYPE html>
<html>

<head>
    <title>Comportamento do gato persa e exótico e a importância da castração - Gatil Hauser</title>
    <meta name="description" content="Apesar de parecidos existe diferença na personalidade do gato persa e do exótico. 
			O gato persa é mais parado, mais tranquilo, enquanto o exótico é mais agitado, mais bagunceiro. 
			Dicas sobre a importância da castração para a saúde do gato e também para melhorar seu temperamento. 
			Gatos castrados são gatos mais saudáveis e mais calmos." />
    <meta name="keywords" content="comportamento do gato persa e exótico, individualidade de cada gato, 
			gato persa tranquilo, temperamento do gato exótico, macho ou fêmea de gato persa e exótico, gatil,
			Gatil Hauser, castração de gato, benefícios da castração dos gatos" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>

    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">

    <script type="text/javascript" charset="utf-8">
    $(function() {
        $('.gallery-top a').Chocolat();
    });
    </script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Comportamento e personalidade</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <h2 class="titulo-texto">Comportamento e personalidade do gato persa e exótico</h2>

                        <p>Um gatinho não é uma mercadoria como um fogão ou uma geladeira. Cada gatinho tem suas
                            qualidades, seus defeitos, sua genética, seu temperamento individual.</p>

                        <p>O gato Persa e o Exótico são muito parecidos. </p>

                        <p>Basicamente as maiores diferenças são: o comprimento dos pelos (o persa tem os pelos longos e
                            o exótico mais curtos) e a energia (o persa é mais calmo, mais parado, e o exótico é um
                            pouco mais agitado, brinca mais, corre mais).</p>

                        <p>Mas, de maneira geral, eles são muito parecidos.</p>

                        <p>O cruzamento entre persas, exóticos e himalaios é permitido, visto que são raças irmãs. Do
                            cruzamento entre persa e exóticos nascem filhotinhos persas e também filhotinhos exóticos.
                            Nunca um filhote nasce misturado. Ou ele é persa, ou ele é exótico.</p>

                        <p>O cruzamento entre persas e himalaios também é permitido, mas na minha opinião não é tão
                            interessante visto que as cores dos pelos são diferentes e a cor dos olhos também é
                            diferente (o himalaio é ponteado com olhos azuis). Se for feito um cruzamento errado os
                            filhotes nascem com cores não aceitas nos padrões das raças e os olhos em tonalidades
                            erradas. Mas nós, do Gatil Hauser, só temos persas e exóticos.</p>

                        <p>Entre o persa e o exótico não ocorrem esses problemas, pois as cores dos pelos são as mesmas
                            nas duas raças e a cor dos olhos também (cor de cobre/alaranjados). É somente o comprimento
                            dos pelos que difere.</p>

                        <h2 class="titulo-texto">Gato Persa</h2>

                        <p>O gato Persa é uma das raças felinas mais populares do mundo. Os gatos dessa raça são
                            medianos (pesando entre 3 kg e 7 kg), possuem uma farta e densa pelagem, focinhos
                            achatados,
                            olhos arredondados e grandes. Além de serem belíssimos também contam com um ótimo
                            temperamento: tranquilos, independentes, dóceis e sociáveis.</p>

                        <p>O Persa é, sem dúvida, uma das raças mais carinhosas, entretanto, é essencial que haja
                            responsabilidade e comprometimento, pois, cuidar de um persa exige tempo, carinho e
                            dinheiro.</p>

                        <p>O cuidado com os pelos e com os olhos deve ser diário. Além disso, os Persas não gostam
                            de
                            passar grandes períodos sozinhos. Se você gosta muito de viajar e está sempre fora de
                            casa,
                            um gato dessa raça não é uma boa escolha, já que ele sofrerá muito com sua ausência.</p>

                        <h2 class="titulo-texto">Temperamento do gato Persa</h2>

                        <p>Dóceis e tranquilos, os Persas são ótimos companheiros. O padrão do gato persa indica que
                            são
                            extremamente dóceis e sensíveis, apegados aos donos e muito fiéis. Ainda assim, são
                            independentes.</p>

                        <p>No geral, os persas não são muito ativos. A convivência com um gato dessa raça pode ser
                            incrivelmente gratificante, visto que são calmos e companheiros. </p>

                        <h2 class="titulo-texto">Cuidando dos Pelos</h2>

                        <p>Os pelos compridos dessa raça exigem cuidados para que continuem bonitos. O ideal é que o
                            persa tenha seus pelos penteados diariamente, principalmente nas regiões que ficam mais
                            cheias de nós, como o tórax, o abdômen, atrás das orelhas e na parte interior das patas.
                            Primeiramente, faça esse processo com um pente de metal. Em seguida, repita com uma
                            escova
                            de cerdas macias para deixar os pelos mais belos e saudáveis.</p>

                        <p>Ainda que os Persas sejam animais extremamente limpos, o ideal é que tomem banho 1 vez
                            por
                            mês ou a cada 2 meses para manter os pelos livres de sujeira e oleosidade, que causam
                            mais
                            nós. Os pelos mortos também causam muitos nós, por isso é muito importante a escovação.
                        </p>

                        <h2 class="titulo-texto">Cuidando dos olhos</h2>

                        <p>O Persa necessita de limpeza diária nos olhos pois as lágrimas escorrem e podem manchar
                            os
                            pelos da face. Isso acontece devido ao formato do focinho, com nariz achatado e as
                            passagens
                            nasais mais estreitas. </p>

                        <h2 class="titulo-texto">Gato Exótico</h2>

                        <p>O gato Exótico foi criado nos Estados Unidos, no final dos anos 50, a partir do
                            cruzamento do
                            gato Persa e do American Short Hair. A intenção era criar um gato que mantivesse as
                            características da raça Persa somadas à facilidade do pelo curto do Short Hair. Essa
                            raça,
                            relativamente nova - só foi reconhecida em 1966, tem conquistado muitos admiradores
                            mundo
                            afora.</p>

                        <p>Os gatos exóticos chamam a atenção pela similaridade com o Gato Persa. Seu diferencial é
                            a
                            pelagem: densa, macia e curta.</p>

                        <p>Para os apaixonados pela personalidade do gato Persa, mas que não dispõem do tempo que a
                            raça
                            requer com higiene e escovação, o gato Exótico é uma ótima opção e essa facilidade se
                            tornou
                            o grande chamariz para a popularização da raça.</p>

                        <p>Também são carinhosos, calmos, amáveis e leais, mas são mais ativos, agitados e
                            brincalhões
                            que os Persas.</p>

                        <h2 class="titulo-texto">Personalidade do Exótico</h2>

                        <p>De temperamento calmo e doce, esse gato também é muito brincalhão.</p>

                        <p>Resultado do cruzamento de duas raças com temperamentos muito distintos, o gato Exótico é
                            calmo, amável e carinhoso como o Persa, mas também pode ser muito ativo como o American
                            Short Hair. Afetuoso e se dá muito bem com outros animais. Adora a companhia do dono,
                            mas
                            fica muito bem quando está sozinho, aproveita para descansar e acumular energia para a
                            próxima brincadeira.</p>

                        <p>Os Exóticos são gatos silenciosos, assim como o persa, quase não vocalizam, raramente
                            miam,
                            muito pacíficos e não são do tipo manhoso.</p>

                        <p>Apesar de adorarem companhia, são brincalhões e uma bolinha de papel já é o suficiente
                            para
                            que brinquem sozinhos até ficarem exaustos e caírem em um sono profundo. Mesmo sendo
                            muito
                            apegados ao dono, os gatos dessa raça lidam bem com a ausência de companhia, além da sua
                            capacidade de se entreterem sozinhos. </p>

                        <p>Apesar de serem bastante calmos, são excelentes caçadores, então não se espante com esse
                            comportamento, apenas garanta que sua casa esteja livre de baratas e ratos, do
                            contrário,
                            seu bichano entrará em ação e essas pestes podem representar um risco à saúde do seu
                            gatinho. </p>

                        <h2 class="titulo-texto">Cuidados e higiene</h2>

                        <p>O Exótico é um gato de trato relativamente simples. Se comparado aos cuidados necessários
                            com
                            os gatos persas, o exótico requer pouco esforço na higiene diária.</p>

                        <p>A pelagem, mesmo curta, deve ser escovada de 2 a 3 vezes por semana. A escovação garante
                            que
                            os pelos fiquem macios e saudáveis, além de promover a limpeza dos fios soltos que podem
                            se
                            embaraçar com a camada mais densa de pelos, formando nós que provavelmente incomodarão.
                            É
                            recomendado usar um pente de aço, facilmente encontrado em pet shops e a rasqueadeira
                            para
                            remover os pelos mortos.</p>

                        <p>Os ouvidos também devem ser limpos uma vez por semana, o ideal é limpar com algodão seco,
                            sem
                            ir fundo para não machucar o gatinho.</p>

                        <p>Os banhos podem ser mensais ou a cada 2 meses, mas os olhos devem ser limpos todos os
                            dias
                            com um algodão umedecido com água, de forma bem delicada. </p>

                        <p style="margin-top:1em;">Fonte: <strong>Agenda Pet</strong>.</p>

                        <h2 class="titulo-texto">Macho ou Fêmea?</h2>

                        <p>Muitas pessoas perguntam se o gatinho macho ou fêmea é mais carinhoso, qual gosta mais de
                            colo, qual é mais independente, entre outras coisas.</p>

                        <p>Em minha opinião não é o sexo do gato que determina o seu jeito de ser.</p>

                        <p>Assim como as pessoas, os animais também têm sua própria personalidade. Nem todas as
                            pessoas
                            são iguais, nem todos os cachorros são iguais e nem todos os gatos são iguais. Cada gato
                            é
                            único e tem seu próprio jeito de ser.</p>

                        <p>Alguns gostam mais de colo, são mais carentes, outros são mais bagunceiros, mais
                            independentes. Portanto não tem como dizer que todas as fêmeas são de um jeito e que
                            todos
                            os machos são de outro jeito. Cada gatinho tem o seu jeito.</p>

                        <p>Mas é possível perceber que eles acostumam com maior contato. Mesmo que quando bebês eles
                            não
                            gostem muito de ficar no colo ou ficar juntinho das pessoas, só querem correr e brincar,
                            às
                            vezes em suas novas casas eles ficam mais próximos porque as pessoas ficam pegando no
                            colo o
                            tempo todo e brincando. Eles acostumam.</p>

                        <p>E, além disso, eles são únicos, tem mais atenção, são o nenê da casa e acabam acostumando
                            com
                            maior contato físico e se tornando mais carinhosos. Entretanto, alguns gostam das
                            pessoas,
                            gostam de estar próximo, no mesmo ambiente da casa, mas não gostam de ficar no colo.
                            Isso
                            não quer dizer que ele não esteja feliz e que não goste do dono. É preciso saber
                            respeitar a
                            personalidade de cada um.</p>

                        <p>Assim sendo, não tem como afirmar se um gatinho será mais carinhoso e ficará no colo o
                            tempo
                            todo ou se será mais independente. Vai depender da personalidade dele e também do modo
                            que
                            será tratado em seu novo lar. Assim como eles podem acostumar a ficar junto das pessoas,
                            eles também podem acostumar a ficar distantes se não receberem a devida atenção.</p>

                        <h2 class="titulo-texto" id="castracao">Benefícios da castração/esterilização</h2>

                        <p>A castração é um ato de amor. Com essa simples atitude você deixará seu animalzinho mais
                            saudável, mais carinhoso, mais tranquilo e ainda evitará animais abandonado no futuro,
                            como
                            tantos que já existem.</p>

                        <h5>Fêmeas</h5>

                        <ul class="topico">
                            <li>
                                <p>Ficam mais calmos, carinhosos;</p>
                            </li>
                            <li>
                                <p>Reduz os riscos de tentativa de fuga;
                            </li>
                            <li>
                                <p>Não passam mais pelo desgaste do cio;</p>
                            </li>
                            <li>
                                <p>Não correm risco de pegar doenças cruzando com gatos doentes;</p>
                            </li>
                            <li>
                                <p>Reduz o risco de tumor mamário, principalmente quando a castração é feita antes do
                                    primeiro cio;</p>
                            </li>
                            <li>
                                <p>Evita o risco de piometria, que é uma infecção no útero;</p>
                            </li>
                            <li>
                                <p>Evita totalmente os riscos de tumores no útero e ovários;</p>
                            </li>
                            <li>
                                <p>Aumenta a expectativa de vida e diminui os riscos de doenças;</p>
                            </li>
                            <li>
                                <p>Evita a “continuidade” de doenças hereditárias.</p>
                            </li>
                        </ul>

                        <h5>Machos</h5>

                        <ul>
                            <li>
                                <p>Ficam mais calmos, carinhosos;</p>
                            </li>
                            <li>
                                <p>Reduz os riscos de tentativa de fuga;
                            </li>
                            <li>
                                <p>Não vão na rua brigar com outros machos;</p>
                            </li>
                            <li>
                                <p>Correm muito menos risco de pegar uma doença;</p>
                            </li>
                            <li>
                                <p>Não correm risco de desenvolverem tumor nos testículos;</p>
                            </li>
                            <li>
                                <p>Diminui consideravelmente o câncer de próstata, as hérnias perineais e a hipertrofia
                                    prostática;</p>
                            </li>
                            <li>
                                <p>O macho castrado provavelmente não vai marcar tanto o território através da urina;
                                </p>
                            </li>
                            <li>
                                <p>Diminui muito o cheiro forte da urina dos gatos;</p>
                            </li>
                            <li>
                                <p>Aumenta a expectativa de vida e diminui os riscos de doenças;</p>
                            </li>
                            <li>
                                <p>Evita a “continuidade” de doenças hereditárias.</p>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>

                    <p style="font-size: 1.2em;"><a HREF="https://www.youtube.com/user/GatilHauser/videos"
                            TARGET="_blank">Clique aqui</a>
                        para assistir mais vídeos no <strong>YouTube</strong>.</p>
                    <p>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/H5pIe9pIlmY" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen>
                        </iframe>
                    </p>

                    <p>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/SPoNuHMDNuM" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen>
                        </iframe>
                    </p>
                    <p>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/bfyEB0oMl24" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen>
                        </iframe>
                    </p>

                    <p id="pkd"><a href="#" class="scroll">Clique aqui para retornar ao início.</a></p>

                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>