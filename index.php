<!DOCTYPE html>
<html>

<head>
    <title>O Gatil Hauser é especializado em gatos persas e exóticos</title>
    <meta name="google-site-verification" content="N9GeyT3yuAMGK6ht7-k5Sqh4TfGpOGlJexQ0XCi-BlY" />
    <meta name="description" content="Você está preparado para ter um gato persa ou exótico? 
		Ter um animal de estimação exige bastante responsabilidade e dedicação, 
		além dos gastos necessários para mantê-lo bonito e saudável." />
    <meta name="keywords" content="Gatil Hauser, gato persa, gato exótico, gatil, criação de gatos de raça,	
		filhote de gato exótico, vídeo de gato, gato" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner">
        <div class="banner-1"> </div>
        <div class="container">
            <div class="banner-top">
                <div class="col-sm-6 head-banner">
                    <h2>Criação e venda de gatos <br> persas e exóticos</h2>
                    <h4 style="color: #202020;">Nossas redes sociais</h4>
                    <a href="https://www.facebook.com/gatilhauser" target="_blank"><button class="button-rede"><img
                                src="images/icones/facebok.png" alt=""></button></a>
                    <a href="https://instagram.com/gatilhauser/" target="_blank"><button class="button-rede"
                            style="margin-left: 30px;"><img src="images/icones/instagram.png" alt=""></button></a>
                    <a href="https://www.youtube.com/user/GatilHauser" target="_blank"><button class="button-rede"
                            style="margin-left: 30px;"><img src="images/icones/youtube.png" alt=""></button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="container">
            <div class="content-top-top">
                <div class="content-top">
                    <div class="col-md-4 wel">
                        <div class="content-ic">
                            <img src="images/icones/filhote.png" />
                        </div>
                        <div class="content-text">
                            <a href="filhotes_de_gato_disponiveis_para_venda.php">
                                <h5> Filhotes disponíveis </h5>
                            </a>
                            <p>Todos os nossos filhotes vendidos para estimação são entregues
                                <strong>castrados</strong>,
                                com pedigree, vacinados e com cópia dos exames de PKD, Fiv e Felv dos pais do gatinho.
                            </p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="col-md-4 wel">
                        <div class="content-ic">
                            <i class="glyphicon glyphicon-usd"></i>
                        </div>
                        <div class="content-text">
                            <a href="reserva_e_valor_do_gato.php">
                                <h5> Reserva e valor </h5>
                            </a>
                            <p>O valor é R$3.600,00 e deverá ser pago em dinheiro ou transferência bancária
                                (30% do valor do gatinho no momento da reserva e 70% dez dias antes da data da entrega +
                                o valor de envio).
                            </p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="col-md-4 wel">
                        <div class="content-ic">
                            <img src="images/icones/comportamento.png" />
                        </div>
                        <div class="content-text">
                            <a href="comportamento_do_gato_e_castracao.php">
                                <h5> Comportamento e castração </h5>
                            </a>
                            <p>Cada gatinho tem suas qualidades, seus defeitos, sua genética e seu temperamento
                                individual.
                                Gato castrado é mais saudável, mais carinhoso e mais tranquilo.
                            </p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="content-top">
                    <div class="col-md-4 wel">
                        <div class="content-ic">
                            <img src="images/icones/doenca.png" />
                        </div>
                        <div class="content-text">
                            <a href="doencas_de_gato.php">
                                <h5> Doenças </h5>
                            </a>
                            <p>São três as doenças que mais assombram os gatos, e também, nós tutores: FeLV, FIV e PKD.
                            </p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="col-md-4 wel">
                        <div class="content-ic">
                            <img src="images/icones/mamaespapais.png" />
                        </div>
                        <div class="content-text">
                            <a href="meus_gatos_mamaes_e_papais.php">
                                <h5> Mamães e papais </h5>
                            </a>
                            <p>Mamães e papais do nosso gatil.</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="col-md-4 wel">
                        <div class="content-ic">
                            <i class="glyphicon glyphicon-list-alt"></i>
                        </div>
                        <div class="content-text">
                            <a href="pedigrees_e_exames_dos_gatos.php">
                                <h5> Pedigrees e exames </h5>
                            </a>
                            <p>Pedigrees dos nossos gatos e exames de FeLV, FIV e PKD.</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>

        <div class="content-middle">
            <div class="col-md-6 content-middle1">
                <img src="images/ab.jpg" class="img-responsive" alt="" style="width: 94%;">
            </div>
            <div class="col-md-6 content-middle2">
                <h3>Sobre nós</h3>
                <p>Nosso principal objetivo é a saúde e felicidade dos nossos gatos e filhotes.
                    Para isso nosso lar foi totalmente adaptado pensando no conforto e bem-estar dos peludinhos.</p>
                <p>Os filhotes permanecem com suas mães por um longo período de tempo e possuem diversos tipos de
                    brinquedos.
                    Vivem em um ambiente saudável, higienizado e propício para seu desenvolvimento.</p>
                <ul>
                    <li style="font-size: 1em;color: #fff;line-height: 1.8em;text-decoration: none;"><i
                            class="glyphicon glyphicon-ok"></i>Nossos gatos são alimentados com ração super premium.
                    </li>
                    <li style="font-size: 1em;color: #fff;line-height: 1.8em;text-decoration: none;"><i
                            class="glyphicon glyphicon-ok"></i>Todo nosso plantel foi testado para as doenças PKD (por
                        DNA), FIV e FELV (por PCR), apresentando resultados negativos.</li>
                    <li style="font-size: 1em;color: #fff;line-height: 1.8em;text-decoration: none;"><i
                            class="glyphicon glyphicon-ok"></i>Todos os filhotes são vendidos já castrados, vacinados e
                        com pedigree.</li>
                    <li style="font-size: 1em;color: #fff;line-height: 1.8em;text-decoration: none;"><i
                            class="glyphicon glyphicon-ok"></i>O Gatil Hauser é filiado ao Clube UNIGAT-BR, no RS.</li>
                </ul>

            </div>

            <!--
            <div>
                <a href="https://www.premierpet.com.br/" target="_blank"><img class="logo-premier"
                        src="images/premiere2.jpg" alt="Premier Pet" /></a>
            </div>
            -->

            <div class="clearfix"></div>
        </div>

        <div class="events">
            <div class="container">
                <h2>Vídeos em destaque</h2>
                <div class="events-top">
                    <div class="col-sm-4 top-event">
                        <a href="">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/IEUVUeu0TV4"
                                frameborder="0" allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                        <h4><a href="">Como adaptar um gato novo em casa?</a></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i>03/05/2022</span>
                        <p> Como acostumar um novo gato com os gatos antigos da casa? Veja neste vídeo como se
                            desenvolve o processo de adaptação.</p>
                    </div>
                    <div class="col-sm-4 top-event">
                        <a href="">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/LrrCuNbWazU"
                                frameborder="0" allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                        <h4><a href="">Gato arranhando o sofá?</a></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i>20/10/2022</span>
                        <p> Veja 6 dicas para evitar que o gato arranhe seu sofá.</p>
                    </div>

                    <div class="col-sm-4 top-event">
                        <a href="">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/MFOciYkRU2E"
                                frameborder="0" allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                        <h4><a href="">Como cuidar de um gato</a></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i>08/12/2022</span>
                        <p> Vai adotar, ganhar um gato e não sabe como cuidar? Ração, vacina, vermífugo, areia e muito
                            mais. </p>
                    </div>
                    <div class="clearfix"> </div>
                </div>

                <div class="events-top">
                    <div class="col-sm-4 top-event">
                        <a href="">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/AQvEa7OmOAQ"
                                frameborder="0" allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                        <h4><a href="">Como deixar um gato feliz dentro de casa?</a></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i>11/05/2022</span>
                        <p>Neste vídeo dou muitas dicas do que nós podemos fazer para que nossos gatos sejam muito
                            felizes e saudáveis vivendo dentro de casa. </p>
                    </div>

                    <div class="col-sm-4 top-event">
                        <a href="">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/Oss6V0_6dCg"
                                frameborder="0" allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                        <h4><a href="">Como educar um gato com mau comportamento?</a></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i>24/07/2017</span>
                        <p> Neste vídeo mostrarei como ensinar e impor limites desde que o gatinho é bem pequeno para
                            que ele não adquira manias erradas.</p>
                    </div>

                    <div class="col-sm-4 top-event">
                        <a href="">
                            <iframe width="640" height="360" src="https://www.youtube.com/embed/RkC28jSsJ0M"
                                frameborder="0" allowfullscreen style="width: 100%;height: 231px;"></iframe></a>
                        <h4><a href="">Dicas para o gato não pular na pia, mesa...</a></h4>
                        <span><i class="glyphicon glyphicon-calendar"></i>13/07/2022</span>
                        <p>Neste vídeo darei dicas de como fazer para que o gato não suba na pia, na mesa ou em qualquer
                            outro lugar. </p>
                        <div class="clearfix"> </div>
                    </div>

                </div>
            </div>
        </div>
</body>

</html>