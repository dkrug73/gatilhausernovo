<!DOCTYPE html>
<html>

<head>
    <title>Filhotes vendidos</title>
    <meta name="description" content="Pedigrees e exames dos gatos adultos, mamães e papais. 
			Exames de PDK, Fiv e Felv atestando que todos nossos gatos são livres dessas doenças seríssimas. 
			Não fazemos exames com kits prontos! Todos os nossos exames de PKD foram feitos por DNA e Fiv e Felv por PCR." />
    <meta name="keywords"
        content="gatil, Gatil Hauser, pedigrees dos gatos persas e exóticos, exames de PKD, Fiv e Felv" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>

    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">

    <script type="text/javascript" charset="utf-8">
    $(function() {
        $('.gallery-top a').Chocolat();
    });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Filhotes vendidos</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <h2 class="titulo-texto">Filhotes vendidos</h2>
                        <p><strong>NENHUM FILHOTE DO GATIL HAUSER É ANUNCIADO EM SITES DE VENDAS (Mercado Livre, OLX
                                etc.). </strong></p>

                        <p>O contato se dá exclusivamente através dos nossos canais, como
                            <strong>FACEBOOK</strong>, <strong>INSTAGRAM</strong>, <strong>EMAIL</strong> ou
                            <strong>YOUTUBE</strong>.
                        </p>

                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="grid">

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (99).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (99).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (98).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (98).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (97).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (97).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (96).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (96).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (95).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (95).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (94).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (94).jpg" alt="Exótica" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (93).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (93).jpg" alt="Exótica" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (92).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (92).jpg" alt="Exótica" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (91).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (91).jpg" alt="Exótica" />
                        </figure>
                    </a>
                </div>
                
                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (90).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (90).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (89).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (89).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (88).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (88).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (87).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (87).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (86).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (86).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (85).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (85).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (80).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (80).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (79).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (79).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (78).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (78).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (77).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (77).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (76).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (76).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (75).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (75).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (74).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (74).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (73).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (73).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (72).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (72).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (71).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (71).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (70).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (70).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (69).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (69).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (68).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (68).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (67).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (67).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (66).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (66).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (65).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (65).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (64).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (64).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (63).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (63).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (62).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (62).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (61).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (61).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (60).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (60).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (59).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (59).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (58).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (58).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (57).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (57).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (56).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (56).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (55).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (55).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (54).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (54).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (53).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (53).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (52).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (52).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (51).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (51).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (50).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (50).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (49).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (49).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (48).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (48).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (47).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (47).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (46).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (46).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (45).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (45).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (44).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (44).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (43).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (43).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (42).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (42).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (41).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (41).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (40).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (40).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (39).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (39).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (38).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (38).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (37).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (37).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (36).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (36).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (35).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (35).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (34).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (34).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (33).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (33).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (32).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (32).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (31).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (31).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (30).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (30).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (29).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (29).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (28).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (28).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (27).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (27).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (26).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (26).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (25).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (25).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (24).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (24).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (23).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (23).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (22).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (22).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (21).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (21).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (20).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (20).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (19).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (19).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (18).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (18).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (17).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (17).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (16).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (16).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (15).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (15).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (14).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (14).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (13).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (13).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (12).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (12).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (11).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (11).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (10).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (10).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (9).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (9).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (8).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (8).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (7).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (7).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (6).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (6).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (5).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (5).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (4).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (4).jpg" alt="Persa" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (3).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (3).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (2).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (2).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/vendidos/persa_exotico (1).jpg" rel="Gatil Hauser"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/vendidos/persa_exotico (1).jpg" alt=Exótica />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
</body>

</html>