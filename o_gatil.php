<!DOCTYPE html>
<html>

<head>
    <title>Instalações do Gatil Hauser - Gatil Hauser</title>
    <meta name="description"
        content="Instalações e adaptações feitas em nossa casa para proporcionar maior diversão e segurança para os gatos." />
    <meta name="keywords" content="gatil;, Gatil Hauser, fotos do gatil, espaço para gatos, ambiente para gatos" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Instalações</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <p>O Gatil Hauser é um pouco diferente de alguns Gatis. Nossos gatos vivem soltos dentro de
                            nossa casa. Não
                            aceitamos a ideia de deixar os gatos vivendo sozinhos em um gatil construído de maneira
                            isolada, sem
                            conviver conosco. Eles são parte da nossa família e nos fazem companhia dentro de casa. É
                            como se fossem
                            nossos filhos.</p>

                        <p>O gatil é constituído de um quarto onde ficam algumas coisas deles, como ração, água,
                            banheiros,
                            arranhadores
                            e prateleiras para se divertirem. Mas geralmente eles estão onde nós estamos. Outro quarto é
                            usado de
                            maneira separada somente quando temos filhotinhos. As mamães ficam com seus bebês
                            recém-nascidos ou
                            quando
                            precisamos separar alguma fêmea e macho porque não está em época de poder cruzar. </p>

                        <p>Todas as janelas da casa possuem grades mandadas fazer com espaçamento de 5 cm entre um ferro
                            e outro
                            para
                            que os gatos não consigam passar para fora. A porta do nosso quarto possui uma portinha pet
                            para que
                            possam
                            entrar e sair quando quiserem. Possuímos arranhadores e caminhas espalhados por todos os
                            cômodos. No
                            pátio
                            dos fundos há muros altos (2,5 metros de altura) para que não consigam pular e grama para
                            que possam
                            ficar
                            soltos, brincar e tomar sol.</p>

                        <h2 class="titulo-texto">Ambientes e instalações</h2>

                        <div class="grid">
                            <div class="col-md-4 gallery-top">
                                <a href="images/instalacoes/quarto_dos_gatos_1.JPG" rel="title"
                                    class="b-link-stripe b-animate-go  thickbox">
                                    <figure class="effect-oscar">
                                        <img src="images/instalacoes/quarto_dos_gatos_1_pq.JPG" alt="Instalações" />
                                    </figure>
                                </a>
                            </div>

                            <div class="col-md-4 gallery-top">
                                <a href="images/instalacoes/quarto_dos_gatos_2.JPG" rel="title"
                                    class="b-link-stripe b-animate-go  thickbox">
                                    <figure class="effect-oscar">
                                        <img src="images/instalacoes/quarto_dos_gatos_2_pq.JPG" alt="Instalações" />
                                    </figure>
                                </a>
                            </div>

                            <div class="col-md-4 gallery-top">
                                <a href="images/instalacoes/quarto_dos_gatos_3.JPG" rel="title"
                                    class="b-link-stripe b-animate-go  thickbox">
                                    <figure class="effect-oscar">
                                        <img src="images/instalacoes/quarto_dos_gatos_3_pq.JPG" alt="Instalações" />
                                    </figure>
                                </a>
                            </div>
                            <div class="clearfix"> </div>
                        </div>

                        <div class="grid">
                            <div class="col-md-4 gallery-top">
                                <a href="images/instalacoes/grama_1.JPG" rel="title"
                                    class="b-link-stripe b-animate-go  thickbox">
                                    <figure class="effect-oscar">
                                        <img src="images/instalacoes/grama_1_pq.JPG" alt="Instalações" />
                                    </figure>
                                </a>
                            </div>

                            <div class="col-md-4 gallery-top">
                                <a href="images/instalacoes/janela_1.JPG" rel="title"
                                    class="b-link-stripe b-animate-go  thickbox">
                                    <figure class="effect-oscar">
                                        <img src="images/instalacoes/janela_1_pq.JPG" alt="Instalações" />
                                    </figure>
                                </a>
                            </div>

                            <div class="col-md-4 gallery-top">
                                <a href="images/instalacoes/janela_2.JPG" rel="title"
                                    class="b-link-stripe b-animate-go  thickbox">
                                    <figure class="effect-oscar">
                                        <img src="images/instalacoes/janela_2_pq.JPG" alt="Instalações" />
                                    </figure>
                                </a>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>