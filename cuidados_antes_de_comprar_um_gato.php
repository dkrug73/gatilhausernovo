<!DOCTYPE html>
<html>

<head>
    <title>Cuidados que você deve ter antes de escolher um filhote de gato - Gatil Hauser</title>
    <meta name="description"
        content="Cuidados que você deve ter antes de escolher um filhote de gato persa ou exótico. 
		Saúde, ração, doenças, vacinas, castração, idade do filhote, ambiente que eles vivem são alguns pontos a serem observados." />
    <meta name="keywords"
        content="escolha do filhote de gato persa exótico, gatil, Gatil Hauser, gato saudável, pedigree, exames,
		preço, ração, cuidado com higiene, vacinas, castração, custo da criação, crias, local que os gatos vivem, idade que os filhotes são entregues" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Cuidados antes da compra</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <h2 class="titulo-texto">Cuidados necessários antes de comprar o seu gato</h2>

                        <p>Todos os filhotes para companhia/estimação são vendidos castrados.</p>

                        <p> Você está procurando um gato para lhe fazer companhia e alegrar sua residência? Você pode, e
                            deve procurar, pesquisar, conversar com os criadores, com veterinários... Mas,
                            independentemente de onde você comprar seu gato persa ou exótico, você não pode deixar de
                            lado alguns cuidados essenciais:</p>

                        <ol>
                            <li>
                                <p>O filhote que você gostou possui pedigree? O Pedigree é “apenas” uma certidão de
                                    nascimento, onde constam o nome do gato, data de nascimento, número de registro
                                    (pedigree), sexo, raça e a cor, os nomes dos pais, dos avós, bisavós. Também consta
                                    o nome do Gatil onde o gato nasceu e o proprietário. Enfim, o pedigree é o documento
                                    que identifica o gato. Porém, é o único certificado que te dá a certeza de que este
                                    gato é um exemplar de raça pura e que não foram feitos cruzamentos com
                                    consanguinidades indesejadas entre os antepassados. Este documento é a garantia da
                                    procedência do animal. Não há por que cobrar mais caro por um pedigree, isso é
                                    obrigação de qualquer bom criador. O documento em si não custa caro. O que custa
                                    mais caro é criar um filhote com qualidade!</p>
                            </li>

                            <li>
                                <p>Não faça sua seleção pelo preço. Gatis que trabalham com qualidade e respeito tanto
                                    pelos animais, quanto por seus clientes, tem um custo alto para oferecer uma
                                    excelente qualidade de vida para seus gatos e filhotes saudáveis. Pessoas que vendem
                                    gatos de raça por um preço muito baixo, tenha certeza que o tratamento dado aos
                                    gatos não é de boa qualidade, pois os custos do Criador não cobririam o valor da
                                    venda do gatinho, pois se gasta muito com ração (muito mesmo), castração, vacinas,
                                    areia higiênica, vermífugo, vitaminas, algodão, gaze, soro fisiológico, xampu, água,
                                    luz, veterinário, exames, etc.</p>

                                <p>Os custos do Criador são contínuos, sempre, o ano todo e não somente quando tem
                                    filhotinhos a venda. Infelizmente tem “criadores” que vendem caro e mesmo assim não
                                    usam ração Super Premium, não fazem os exames necessários e nem entregam castrados.
                                </p>

                                <p>Muitas pessoas não se dão conta das consequências que terão em comprar um gatinho sem
                                    tomar alguns cuidados. Infelizmente, somente após estar com o gatinho em casa que
                                    esses problemas aparecerão e, então, terão que gastar muito mais tratando dos
                                    problemas de saúde do gatinho, o que geralmente acaba custando muito mais do que o
                                    preço de um bom filhote, e, algumas vezes o gatinho acaba morrendo dependendo da
                                    doença pré-existente.</p>
                            </li>

                            <li>
                                <p>Pergunte sobre a ração que os pais e os filhotes comem, qual é a marca e se é Super
                                    Premium. Verifique se a ração de qualidade é dada durante todo o ano, para todos os
                                    gatos, ou somente quando a gata tem cria. A alimentação dos gatos e dos filhotes
                                    determina em grande parte a saúde deles. Um gatinho que foi mal alimentado enquanto
                                    bebê será muito mais propenso a ter doenças. Alguns dão ração de qualidade inferior
                                    para diminuir os custos, "afinal são muitos gatos para alimentar...". O importante é
                                    verificar se é Super Premium. Pergunte se é dado algum reforço na alimentação, como
                                    carne moída, frango cozido, sachês para gatos.</p>
                            </li>

                            <li>
                                <p>Pergunte quantos gatos adultos, fixos (matrizes e padreadores) o Gatil tem. Existem
                                    Gatis que tem muitos (muitos mesmo) gatos fixos para, assim, ter muitos filhotes e
                                    vender bastante e, consequentemente, ter mais renda. Entretanto, quem trabalha com
                                    esse pensamento (produzir, vender, produzir, vender, produzir, vender...) será que
                                    consegue cuidar dos gatos com muita qualidade? Um Gatil correto pode ter no máximo 2
                                    ninhadas por ano de cada gata (é aconselhável que seja mais espaçado porque não dá
                                    tempo suficiente para a gata se recuperar). Um “criador” que não tem amor aos seus
                                    próprios gatos, que só pensa em cruzar e vender filhotes o tempo todo, provavelmente
                                    também não está preocupado com a saúde deles. O pensamento de alguns é simples: “ter
                                    muito filhotinhos para vender bastante e ganhar dinheiro".</p>
                            </li>

                            <li>
                                <p>Visite. Se você está interessado em um filhote, peça para visitar o gatil. Observe se
                                    o local onde os gatos vivem é limpo e bem protegido no inverno, se eles não passam
                                    frio, se não dormem no chão, não ficam num local úmido... Gatos que vivem expostos
                                    ao tempo, a umidade, podem ter problemas de saúde e problemas respiratórios.
                                    Verifique se não estão com fungos na pele, pulgas, otite, espirrando com muita
                                    frequência e secreção no nariz, diarreia, etc. Também observe se os gatos convivem
                                    com as pessoas, tem atividades de diversão e socialização. Se eles recebem carinho,
                                    atenção. Gatos criados em locais isolados das pessoas geralmente são gatos mais
                                    ariscos, arredios, medrosos e assustados.</p>
                            </li>

                            <li>
                                <p>Desconfie se o criador prometer coisas demais. Alguns criadores tentam enganar as
                                    pessoas falando que seus gatos são os melhores, são os mais perfeitos, são os mais
                                    carinhosos, são os mais fofos, são tudo de bom que possa existir. Mas a realidade é
                                    que cada gato é um gato. Cada gato tem sua personalidade e também o seu padrão.
                                    Alguns são mais quietos, mais carinhosos, outros são mais bagunceiros, mais
                                    independentes... </p>
                                <p class="item">Criador que fala somente o que o cliente gostaria de ouvir é um vendedor
                                    e não criador.</p>
                            </li>

                            <li>
                                <p>Não compre filhotes muito novos. Alguns “criadores” querem entregar o filhote rápido
                                    para não ter mais custos com ele (alguns não fazem nem a 1ª vacina, entregam com 45
                                    dias de vida). Mas é importante saber que os gatinhos mamam quase até os 3 meses de
                                    idade e a separação da mãe gera diversos problemas que o acompanham por toda a vida,
                                    principalmente em relação ao comportamento/socialização. Nenhum filhote pode ser
                                    entregue antes dos 3 meses.</p>
                            </li>

                            <li>
                                <p>Obrigatoriamente, peça para ver os exames de PKD, Fiv e Felv do pai e da mãe do
                                    gatinho. Não acredite apenas na palavra de quem diz que fez. Se o criador realmente
                                    fez não vai se importar de mostrar. Tenha certeza que está tudo bem, que eles são
                                    saudáveis. Peça para ver, peça Xerox ou uma cópia por e-mail. São doenças
                                    gravíssimas e que infelizmente não tem cura. Se você adquirir um gatinho com uma
                                    dessas doenças, cedo ou tarde, ele irá ficar muito doente e irá morrer sem que você
                                    possa fazer nada para ajudá-lo.</p>
                            </li>

                            <li>
                                <p>Verifique o que está incluso no preço do gatinho. Obviamente um gatinho que come
                                    ração popular, não faz vacina alguma, não é entregue castrado, não toma vermífugo,
                                    não tem exames de saúde, muitas vezes nem mesmo pedigree, entre outras coisas, custa
                                    muito menos. Pergunte se o filhote terá pedigree, quantas vacinas ele terá feito, se
                                    já é entregue castrado, vermifugado, veja se o criador tem os exames de PKD, Fiv e
                                    Felv dos pais do filhote, se o gatinho não está com fungo, otite...</p>
                            </li>

                            <li>
                                <p>Outra coisa importante para você pensar é sobre a sua liberdade de sair, viajar,
                                    passar as férias longe de casa. Gato não é como cachorro. Gato foge. Gato estranha.
                                    Não dá como levar ele para viajar para um lugar que não tenha redinhas de proteção
                                    ou que as pessoas deixam a porta aberta, por exemplo. Com gatos a situação é
                                    diferente. Os cuidados precisam ser redobrados. O gato não pode sair na rua. Tem que
                                    ser criado somente dentro de casa.</p>

                                <p>Repito: a primeira coisa a fazer quando você comprar/adotar seu gatinho é colocar
                                    redinhas de proteção em todas as aberturas para criar seu gato de maneira segura.
                                </p>

                                <p>Gato não gosta muito de sair de casa. Eles são acostumados com o ambiente deles e se
                                    estressam um pouco em ficar viajando, indo para lugares diferentes. E na maioria das
                                    vezes as outras pessoas não entendem esse cuidado que tem que ter em manter as
                                    portas fechadas, não deixar o gato sair. Por isso quando se está na casa de outra
                                    pessoa o risco de um acidente e o gato escapar é muito grande.</p>

                                <p>E se a opção for deixar ele em casa e pedir para alguém ir cuidar (o que na minha
                                    opinião é o melhor para ele), só irá funcionar se for por poucos dias. Um final de
                                    semana tudo bem, eles ficam numa boa, mas se for passar muito tempo longe eles se
                                    sentem muito sozinhos, eles sentem falta do dono.</p>

                                <p>Por isso eu digo que ter um gato é quase como ficar preso em casa para sempre rss.
                                </p>

                                <p>Você poderá levar seu gato para sua casa de praia, casa de férias se lá você puder
                                    manter a mesma segurança para ele como você tem na casa de cidade. Portas sempre
                                    fechadas e redinhas de proteção em tudo.</p>
                            </li>
                        </ol>

                        <p>É muito importante pensar em tudo isso antes de adquirir seu gatinho. É um grande compromisso
                            que você irá assumir com ele, com a vida dele!</p>

                        <p>Para quem viaja bastante, costuma passar as férias longe de casa, talvez seja melhor um
                            cachorro. Cachorro é mais sociável, não se assusta tanto, não é tão desconfiado e se adapta
                            melhor as mudanças. </p>

                        <p>Faça suas anotações e converse com seu veterinário de confiança. E não se esqueça do velho
                            ditado que sempre funciona: “o barato sai caro.” Com certeza não vale a pena economizar um
                            pouco na compra para gastar muito mais em veterinário e remédios logo nos primeiros meses e
                            ainda ver o gatinho sofrendo, doente e você passar trabalho fazendo tratamentos. Pessoas que
                            compram bichinhos sem ter esses cuidados acabam incentivando a continuidade desse tipo de
                            criação de má qualidade.</p>
                    </div>

                    <p id="pkd"><a href="#" class="scroll">Clique aqui para retornar ao início.</a></p>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    </div>
</body>

</html>