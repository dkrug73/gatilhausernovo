<!DOCTYPE html>
<html>

<head>
    <title>Pedigrees e exames do plantel - Gatil Hauser</title>

    <meta name="google-site-verification" content="N9GeyT3yuAMGK6ht7-k5Sqh4TfGpOGlJexQ0XCi-BlY" />
    <meta name="description" content="Pedigrees e exames dos gatos adultos, mamães e papais. 
        Exames de PDK, Fiv e Felv atestando que todos nossos gatos são livres dessas doenças seríssimas. 
        Não fazemos exames com kits prontos! Todos os nossos exames de PKD foram feitos por DNA e Fiv e Felv por PCR." />
    <meta name="keywords" content="Gatil Hauser, gato persa, gato exótico, gatil, criação de gatos de raça,	
		filhote de gato exótico, vídeo de gato, gato" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Pedigrees e Exames</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <h2 class="titulo-texto">Pedigrees e Exames do plantel do Gatil Hauser</h2>

            <p>O Gatil Hauser existe desde outubro de 2011.</p>
            <p>Inicialmente foi filiado ao Feline Club, de SP. Atualmente é filiado ao UNIGAT-BR, no RS, visto que o
                Gatil Hauser é no Rio Grande do Sul.</p>

            <h2 class="titulo-texto" style="padding-top: 40px;">Certificado de registro do gatil</h2>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Registro_Unigat.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/exames/Registro_Unigat.jpg" title="Registro Unigat-BR"
                                alt="Registro Unigat-BR" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

            <h2 class="titulo-texto">Pedigree e exames de PKD, FIV e Felv do Joey</h2>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Pedigree_Unigat_Joey.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Pedigree_Unigat_Joey-pq.jpg" alt="Pedigree do Joey"
                                title="Pedigree do Joey" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Exame_FivFelv_Joey.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Exame_FivFelv_Joey-pq.jpg" alt="Pedigree do Joey"
                                title="Pedigree do Joey" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

            <h2 class="titulo-texto">Pedigree e exames de PKD, FIV e Felv da Mel</h2>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Pedigree_Unigat_Mel.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Pedigree_Unigat_Mel_pq.jpg" alt="Pedigree da Mel"
                                title="Pedigree da Mel" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Exame_PKD_Mel.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Exame_PKD_Mel_pq.jpg" alt="Pedigree da Mel"
                                title="Pedigree da Mel" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

            <h2 class="titulo-texto">Pedigree e exames de PKD, FIV e Felv da Yumi</h2>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Pedigree_Unigat_Yumi.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Pedigree_Unigat_Yumi_pq.jpg" alt="Pedigree da Yumi"
                                title="Pedigree da Yumi" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Exame_PKD_Yumi.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Exame_PKD_Yumi_pq.jpg" alt="Pedigree da Yumi"
                                title="Pedigree da Yumi" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

            <h2 class="titulo-texto">Pedigree e exames de PKD, FIV e Felv da Zoey</h2>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Pedigree_Unigat_Zoey.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Pedigree_Unigat_Zoey_pq.jpg" alt="Pedigree da Zoey"
                                title="Pedigree da Zoey" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Exame_PKD_Zoey.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Exame_PKD_Zoey_pq.jpg" alt="Pedigree da Zoey"
                                title="Pedigree da Zoey" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

            <h2 class="titulo-texto">Pedigree e exames de PKD, FIV e Felv da Nina</h2>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Pedigree_Unigat_Nina.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Pedigree_Unigat_Nina_pq.jpg" alt="Pedigree da Nina"
                                title="Pedigree da Nina" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/exames/Exame_PKD_Nina.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar" style="box-shadow: 0 0 8px #666; padding-bottom: 0px;">
                            <img src="images/exames/Exame_PKD_Nina_pq.jpg" alt="Pedigree da Nina"
                                title="Pedigree da Zoey" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>
            
        </div>
    </div>
</body>

</html>