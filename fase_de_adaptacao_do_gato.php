<!DOCTYPE html>
<html>

<head>
    <title>Fase de adaptação na chegada do novo gatinho - Gatil Hauser</title>
    <meta name="description"
        content="Todos os gatos estranham a mudança de casa e é necessário passar por uma fase de adaptação 
			de alguns dias para que o filhote se adapte ao novo ambiente e também a nova família. Aqui dou algumas dicas de como ajudar esse processo." />
    <meta name="keywords" content="adaptação de um novo gato, como facilitar a adaptação do gato, gato estranha mudanças, 
			adaptação do gato a nova casa, como apresentar o gato a outro animal, gatil, Gatil Hauser" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Fase de adaptação</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <h2 class="titulo-texto">A fase de adaptação</h2>

                        <p>Chegando a um novo lar todo gatinho precisa de algum tempo até que se sinta à vontade no
                            ambiente e torne-se integrado à família. Esse tempo varia muito de gato para gato. De
                            maneira geral, em aproximadamente duas semanas seu gatinho estará perfeitamente integrado.
                        </p>

                        <p>Se a sua casa é muito movimentada, o ideal é que o filhote fique em um cômodo separado por
                            duas semanas para melhor adaptação. Esse cômodo pode ser um quarto. Importante é que seja
                            fechado e tranquilo, livre de ruídos ou movimentação de muitas pessoas. Deixe nesse quarto a
                            água, a ração próximos e o banheiro dele.</p>

                        <p>Quando chegar com o filhote apenas abra a caixinha de transporte e deixe ela no chão. Não
                            force o gatinho a sair, deixe-o quieto e ele sairá para explorar o cômodo quando tiver
                            vontade. É natural que o filhote se esconda sob móveis e demonstre insegurança, é necessário
                            deixar que ele se acostume aos novos cheiros, ruídos e tenha tempo para examinar o local.
                            Gatos tem obsessão por controle de território e não se sentem seguros em locais
                            desconhecidos, quando ele estiver mais íntimo do novo território voltará a agir com
                            naturalidade e autoconfiança.</p>

                        <p>Se você já possui outros animais é interessante que eles se vejam de longe e vão se
                            acostumando com os cheiros. Não é aconselhável deixá-los juntos nos primeiros dias, pois
                            poderão se estranhar e brigar.</p>

                        <p>Durante os primeiros dias é comum que o filhote não se alimente muito e beba pouca água. É
                            importante que você mantenha a ração indicada por algumas semanas até o filhote estar bem
                            adaptado. Depois poderá substituir por outra da sua preferência (sempre ração Super
                            Premium).</p>

                        <p>Durante os primeiros dias de adaptação ao novo lar também é muito comum que o filhote evite
                            fazer xixi e cocô, caso aconteça não se apavore, é um comportamento natural muito típico dos
                            felinos. Após adaptados ao novo ambiente essas alterações comportamentais dos gatinhos
                            desaparecem, tudo volta ao normal.</p>

                        <p>Ocorrendo com frequência muito menor, mas ainda como resposta à troca de ambiente, podem
                            haver mudanças na consistência das fezes de um filhote. Podem apresentar-se mais secas caso
                            o filhote esteja evitando usar o banheirinho por muito tempo, ou então ficar amolecidas em
                            decorrência do stress causado pela sensação de insegurança.</p>

                        <p> Em ambos os casos a consistência das fezes deverá voltar ao normal até o final das duas
                            semanas de adaptação sem oferecer qualquer prejuízo à saúde do filhote. Casos mais extremos
                            são muito raros, mas caso note que o filhote começou a perder peso ou mostra sinais de
                            fraqueza e apatia leve em um veterinário de confiança.</p>

                        <h2 class="titulo-texto">Apresentando o filhote a outros animais</h2>

                        <p> Não segure o gato a força ou no colo para apresenta-lo a outros gatos ou cães. Isso irá
                            piorar a situação e poderá deixar o gato assustado e bravo. Apenas solte-o no ambiente com
                            os outros animais e observe para interferir apenas se houver brigas, agressão física. Eles
                            irão se cheirar e poderão fazer sons de xingamento uns aos outros, mas assim eles irão se
                            conhecer. Até esse ponto não interfira. Não os deixe sozinhos no início. Caso haja brigas
                            volte com o gatinho para o seu cômodo de início e aguarde mais uns dias.</p>

                        <div class="clearfix"></div>

                        <p style="font-size: 1.2em;"><a HREF="https://www.youtube.com/user/GatilHauser/videos"
                                TARGET="_blank">Clique aqui</a>
                            para assistir mais vídeos no <strong>YouTube</strong>.</p>

                        <h2 class="titulo-texto">Adaptação de um novo gato</h2>

                        <figure>
                            <div class="boxVideo">
                                <iframe width="640" height="360" src="https://www.youtube.com/embed/IEUVUeu0TV4"
                                    frameborder="0" allowfullscreen></iframe>
                            </div>
                        </figure>

                        <p id="pkd"><a href="#" class="scroll">Clique aqui para retornar ao início.</a></p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>