<!DOCTYPE html>
<html>

<head>
    <title>Doenças que acometem alguns gatos: PKD, Fiv e Felv - Gatil Hauser</title>
    <meta name="google-site-verification" content="N9GeyT3yuAMGK6ht7-k5Sqh4TfGpOGlJexQ0XCi-BlY" />
    <meta name="description" content="Doenças que acometem alguns gatos: PKD, Fiv e Felv. 
			Existem algumas doenças muito graves e que infelizmente não tem cura. 
			Só é possível saber se o gato tem essas doenças através de exames de sangue do 
            filhote ou do plantel. 
			Por isso é fundamental que os criadores façam os exames em todos seus gatos adultos. 
            Adultos saudáveis geram bebês saudáveis." />
    <meta name="keywords" content="PKD, Fiv, Felv, doenças de gatos" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>FIV, FeLV e PKD</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <p>São três as doenças que mais assombram os gatos persas e, também, nós tutores:<a href="#fe"
                                class="scroll"> FeLV</a>, <a href="#fi" class="scroll">FIV</a> e <a href="#pk"
                                class="scroll">PKD</a></p>

                        <p>A <strong>FeLV</strong> (Vírus da Leucemia Felina) e o <strong>FIV</strong> (Vírus da
                            Imunodeficiência Felina) estão entre os causadores de doenças infecciosas mais comuns em
                            gatos. São causadas por 2 diferentes tipos de retrovírus, pertencentes ao gênero dos
                            <strong>Oncornavírus</strong> (FeLV) e ao gênero dos <strong>Lentivirus</strong> (FIV),
                            sendo que o FIV pertence à mesma família do vírus causador da imunodeficiência humana
                            (AIDS).
                        </p>

                        <p>A infecção por algum destes vírus compromete o sistema imunológico do animal hospedeiro,
                            interferindo na sua capacidade de combater infecções, predispondo o organismo a uma
                            variedade de doenças secundárias recidivantes ou persistentes.</p>

                        <h2 class="titulo-texto" id="fe">FeLV</h2>

                        <p>O vírus causador da leucemia viral felina pode ser transmitido através da saliva, secreções
                            nasais e lacrimais, urina e fezes de gatos portadores. Um gato saudável pode se infectar
                            após lambedura mútua com outro doente ou através de fômites. Os filhotes de gatas infectadas
                            também podem nascer infectados por meio de contaminação transplacentária ou adquirir o vírus
                            durante a amamentação. Cerca de 80% dos filhotes que adquirem o vírus nestas condições
                            morrem na fase fetal ou neonatal, e os que resistem podem manter-se em viremia persistente.
                        </p>

                        <p>Após a infecção por FeLV, gatos com o sistema imunológico competente podem combater e
                            eliminar o vírus no estágio inicial. Já os animais com o sistema imunológico debilitado
                            permanecem contaminados, dando origem a diversas complicações sistêmicas, como infecções
                            secundárias, alterações hematológicas e até neoplasias.</p>

                        <p>A evolução da doença pode ser classificada em categorias de acordo com a característica da
                            patogenia:</p>

                        <ol>
                            <li>
                                <p><strong>Regressiva</strong> (viremia transitória ou latente): devido a uma resposta
                                    imune eficiente, observado em 30% dos gatos sadios expostos ao FeLV. Podem
                                    apresentar testes positivos na fase inicial, mas tornam-se negativos posteriormente,
                                    pois o organismo neutraliza o vírus.</p>
                            </li>

                            <li>
                                <p><strong>Progressiva</strong> (viremia persistente): devido à falha no desenvolvimento
                                    de uma resposta imune efetiva. Geralmente estes animais desenvolvem sintomas e
                                    apresentam testes positivos.</p>
                            </li>

                            <li>
                                <p><strong>Latência</strong>: o vírus sai da circulação sanguínea, porém permanece
                                    sequestrado na medula óssea, replicando-se sem deixar as células, podendo ser
                                    responsáveis pelo desenvolvimento de anemias e neoplasias. Podem apresentar testes
                                    sorológicos negativos.</p>
                            </li>
                        </ol>

                        <p>Gatos portadores assintomáticos e aparentemente saudáveis podem transmitir FIV e FeLV por não
                            apresentarem sinais clínicos da doença durante semanas, meses ou até mesmo anos após o
                            contágio inicial, tornando-se fontes potencialmente contagiantes para outros indivíduos
                            contactantes.</p>

                        <p>Os sinais clínicos estão associados às infecções secundárias e à imunossupressão como:</p>

                        <ul class="topico">
                            <li>
                                <p>Halitose devido a gengivites ou estomatites</p>
                            </li>
                            <li>
                                <p>Dermatites recorrentes e abscessos
                            </li>
                            <li>
                                <p>Otites</p>
                            </li>
                            <li>
                                <p>Infecções das vias aéreas</p>
                            </li>
                            <li>
                                <p>Enterites</p>
                            </li>
                            <li>
                                <p>Anemia não regenerativa</p>
                            </li>
                            <li>
                                <p>Leucopenia com neutropenia, linfopenia e trombocitopenia ou leucocitose por
                                    linfocitose</p>
                            </li>
                            <li>
                                <p>Linfoma</p>
                            </li>
                            <li>
                                <p>Fibrossarcoma</p>
                            </li>
                            <li>
                                <p>Doenças mieloproliferativas</p>
                            </li>
                        </ul>

                        <br />
                        <figure>
                            <div class="boxVideo">
                                <iframe width="640" height="360" src="https://www.youtube.com/embed/v4eWwYcYQ_o"
                                    frameborder="0" allowfullscreen></iframe>
                            </div>
                        </figure>

                        <p id="pkd"><a href="#" class="scroll">Clique aqui para retornar ao início.</a></p>

                        <h2 class="titulo-texto" id="fi">FIV</h2>

                        <p>A <strong>FIV</strong> é responsável apenas pela doença específica dos felinos, não existindo
                            qualquer risco de infecção para o humano. São vírus frágeis e instáveis no meio ambiente,
                            sendo facilmente inativados pelo calor ou por desinfetantes domésticos e detergentes comuns,
                            sem necessidade de utilização do vazio sanitário prolongado.</p>

                        <p>O gato contrai o vírus da imunodeficiência felina através da saliva, quando é mordido ou
                            arranhado por um gato infectado, ou através do contato sexual durante a cópula. As fêmeas
                            podem transmitir o vírus aos filhotes por via transplacentária ou através da amamentação se
                            forem infectadas antes da gestação.</p>

                        <p>Os sintomas da FIV na fase inicial caracterizam-se por febre, aumento dos gânglios linfáticos
                            e aumento da susceptibilidade às infecções intestinais e cutâneas por um período de 4 a 6
                            semanas após o contagio. Animais jovens ou com um sistema imunológico competente podem
                            apresentar uma fase latente ou subclínica na qual não se observam sinais da doença durante
                            anos. Com o passar do tempo e o avanço da idade os gatos contaminados tendem a apresentar um
                            severo comprometimento do seu sistema imunológico tornando-se susceptíveis a uma grande
                            variedade de infecções crônicas.</p>
                        <p>Os sinais clínicos da FIV podem apresentar 5 estágios distintos:</p>

                        <ol>
                            <li>
                                <p><strong>Fase aguda</strong>: inicia-se de 4-6 semanas pós infecção com
                                    desenvolvimento de febre, leucopenia, esplenomegalia e hepatomegalia. Cerca de 4
                                    meses pós infecção pode ser encontrada hipergamaglobunemia devido ativação
                                    policlonal inespecífica dos linfócitos B. Os anticorpos contra FIV só começam a ser
                                    produzidos pelo sistema imunológico cerca de 3 a 6 semanas após o contágio e
                                    tornam-se detectáveis por teste a partir de 4 a 8 semanas.</p>
                            </li>

                            <li>
                                <p><strong>Fase subclínica</strong>: animal geralmente sem sintomas, podendo apresentar
                                    neutropenia e linfopenia.</p>
                            </li>

                            <li>
                                <p><strong>Fase clínica</strong>: gatos podem demonstrar linfoadenopatia generalizada,
                                    febre, inapetência e perda de peso.</p>
                            </li>

                            <li>
                                <p><strong>Fase crônica</strong>: com manifestações secundárias, como o desenvolvimento
                                    de lesões em cavidade oral, infecção no trato respiratório, febre, diarreia,
                                    alterações hematológicas (anemia, linfopenia, neutropenia e trombocitopenia),
                                    neoplasias e alterações neurológicas.</p>
                            </li>

                            <li>
                                <p><strong>Fase terminal</strong>: devido à extrema debilidade do organismo, não têm
                                    controle sobre as infecções secundárias e podem desenvolver doenças sistêmicas
                                    severas (falência renal, hepática, pancreática ou linfoma).</p>
                            </li>
                        </ol>

                        <br />

                        <figure>
                            <div class="boxVideo">
                                <iframe width="640" height="360" src="https://www.youtube.com/embed/PiRyh_uGFXA"
                                    frameborder="0" allowfullscreen></iframe>
                            </div>
                        </figure>

                        <p id="pkd"><a href="#" class="scroll">Clique aqui para retornar ao início.</a></p>

                        <h2 class="titulo-texto" id="pk">PKD</h2>

                        <p>PKD (do inglês "Polycystic Kidney Disease"), ou Doença Renal Policística, é uma uma das
                            causas mais comuns de insuficiência renal crônica e é também a principal doença hereditária
                            em felinos.</p>

                        <h2 class="titulo-texto">O que PKD causa nos gatos?</h2>

                        <p>A doença provoca o surgimento de cistos no rim, o que causa disfunção renal. A formação dos
                            cistos ocorre ainda no período gestacional, porém estes aumentam de tamanho com o passar do
                            tempo, e podem variar de 1 mm a 1 cm de diâmetro.</p>

                        <p>Normalmente animais mais velhos apresentam cistos maiores e em maior quantidade que animais
                            mais jovens.</p>

                        <p>Os sinais clínicos da doença (depressão, perda ou redução do apetite, sede demasiada, urina
                            excessiva e perda de peso, entre outros), manifestam-se tardiamente, geralmente entre três e
                            dez anos de idade, quando o quadro de insuficiência renal começa a se instalar.</p>

                        <h2 class="titulo-texto">Qual a chance de um gato ter PKD?</h2>

                        <p>O <strong>PKD</strong> é uma doença provocada por herança não ligada ao sexo (pode atingir
                            igualmente machos e fêmeas.), autossômica dominante (sempre que um filhote desenvolver os
                            cistos, um de seus pais também apresentará a doença), e que se expressa tanto em homozigose
                            quanto em heterozigose.</p>

                        <p>Entretanto, acredita-se que a maioria dos embriões homozigotos (portadores dos dois
                            cromossomos com o gene PKD1 contendo a mutação para desenvolvimento de cistos, um derivado
                            do pai e um da mãe) seja abortada ainda no começo de seu desenvolvimento, ou que morra em
                            poucas semanas. Portanto, espera-se que a grande maioria dos gatos portadores da PKD possua
                            um alelo normal (forma selvagem do gene) e um alelo alterado (forma contendo a mutação).</p>

                        <p>Assim, se somente um dos pais for PKD-positivo, a chance de um dos filhotes ser acometido de
                            PKD é de 50%.</p>

                        <p>Por outro lado, se ambos os pais forem PKD-negativo, a chance do filhote ter PKD é nula!</p>

                        <h2 class="titulo-texto">Como se faz o diagnóstico de PKD?</h2>

                        <p>O diagnóstico pode ser feito de maneira nada agressiva, por meio de ultra-sonografia, ou
                            através de exames de DNA. Aos 10 meses de idade o exame anatômico chega a 98% de acurácia,
                            quando realizado por veterinário experiente. Os exames de DNA têm 100% de confiabilidade, em
                            qualquer idade.</p>

                        <p>Também é possível garantir que filhotes sejam PKD-free (não acometidos da doença) pelo
                            diagnóstico PKD-negativo dos pais.</p>

                        <h2 class="titulo-texto">Qual é o tratamento para PKD?</h2>

                        <p>Como qualquer doença genética, a PKD não tem cura. Tratamentos paleativos e preventivos podem
                            ser realizados com o objetivo de evitar a progressão da insuficiência renal crônica e de
                            melhorar a qualidade e o tempo de vida dos animais doentes. Dietas balanceadas, e água em
                            abundância são recomendadas.</p>

                        <p>Devido à gravidade da PKD, o diagnóstico precoce é de extrema importância para que se inicie
                            a terapia preventiva antes que os sinais clínicos de insuficiência renal crônica se
                            estabeleçam.</p>

                        <h2 class="titulo-texto">Como eliminar a PKD de uma população de gatos?</h2>

                        <p>Devido ao caráter hereditário da PKD, os animais utilizados para reprodução devem ser
                            selecionados pelos criadores e proprietários visando o controle e a erradicação desta doença
                            na espécie felina. Gatos que possuem o alelo mutado devem ser castrados e, consequentemente,
                            retirados de programas de reprodução dos criadores.</p>

                        <p>Embora o manejo seja facilitado pela herança da doença ser autossômica dominante (o que
                            significa que todo portador desenvolve a patologia), os exames para certificação do plantel
                            devem ser iniciados antes da idade reprodutiva do animal, e devem ser preferencialmente
                            feitos por meio de análises de DNA, pois cistos muito pequenos podem passar desapercebidos
                            nos exames morfológicos, gerando falsos-negativos, podendo fazer com que um gato seja usado
                            como reprodutor em uma criação durante anos antes que seja diagnosticado com a doença do rim
                            policístico.</p>

                        <p>Estima-se que cerca de 35% dos Persas sejam portadores do gene para PKD. Todas as raças
                            derivadas ou portadoras de linhagens de sangue do persa (ex: Himalaio e Exótico), bem como o
                            próprio Persa, apresentam maior propensão à doença (prevalência estatística). Matrizes e
                            Padreadores destas raças devem, preferencialmente, apresentar exames negativos para PKD, que
                            indiquem ausência da mutação.</p>

                        <h2 class="titulo-texto">Como você pode ajudar a eliminar a PKD?</h2>

                        <p>Assim como o criador precisa estar atento e realizar exames nos gatos do plantel, o comprador
                            de um filhote, sobretudo persa, deve estar também atento a existência de tais exames. Eles
                            podem garantir que seu mascote passe mais tempo ao seu lado e tenha uma vida mais tranquila.
                        </p>

                        <p>Caso você já tenha um gato e tenhas dúvidas sobre a possibilidade de ele ter PKD, faça o
                            diagnóstico. Se positivo, castre imediatamente o seu animal.</p>

                        <p>E antes de comprar um filhote, exija o exame PKD-negativo (por DNA) dos seus pais. Deste
                            modo, você contribuirá para a redução da ocorrência da PKD em gerações futuras.</p>

                        <br />

                        <figure>
                            <div class="boxVideo">
                                <iframe width="640" height="360" src="https://www.youtube.com/embed/gbcyd6Lsw1A"
                                    frameborder="0" allowfullscreen></iframe>
                            </div>
                        </figure>

                        <p style="margin-top:1em;">* Textos da Internet *</p>

                        <p style="font-size: 1.2em;"><a HREF="https://www.youtube.com/user/GatilHauser/videos"
                                TARGET="_blank">Clique aqui</a>
                            para assistir mais vídeos no <strong>YouTube</strong>.</p>

                        <p id="pkd"><a href="#" class="scroll">Clique aqui para retornar ao início.</a></p>

                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>