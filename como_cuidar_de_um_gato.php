<!DOCTYPE html>
<html>

<head>
    <title>Dicas e recomendações de como cuidar do seu gato - Gatil Hauser</title>
    <meta name="description"
        content="Dicas sobre alimentação correta para o seu gato, cuidados de higiene, vacinas, 
			segurança, areia, arranhador para seu gato ter uma vida feliz e saudável. Indicação de produtos que eu uso e gosto." />
    <meta name="keywords" content="ração super premium para gatos persas e exóticos, areia para gatos, 
			pente e rasqueadeira para gatos persas e exóticos, vermífugo para gatos, arranhadores, redes de proteção para gatos,
			pratinhos corretos para gatos, vacinas em gatos, limpar os olhos do gato persa exótico, gatil, Gatil Hauser" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>

    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">

    <script type="text/javascript" charset="utf-8">
    $(function() {
        $('.gallery-top a').Chocolat();
    });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Recomendações e produtos</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <div class="blog-top">
                <div class=" blog-grid2">
                    <div class="blog-text">
                        <h2 class="titulo-texto">Como cuidar de um gato: recomendações e produtos</h2>

                        <p>A alimentação correta do seu gato, desde filhote, irá influenciar a saúde dele por toda a
                            vida. Os gatos não tendo uma boa alimentação, não poderão se desenvolver bem, nem terão a
                            resistência necessária para evitar doenças e ao ambiente. A alimentação básica do gato persa
                            consiste em ração <strong>SUPER PREMIUM</strong>, que são as melhores e água. Nunca dê leite
                            para o
                            gato, pois certamente causará diarreia. A ração deve ser deixada à disposição do gato, assim
                            como a água, sempre fresca. A maioria das rações baratas podem causar problemas para o gato.
                            As rações Super Premium evitam vários problemas, sem contar que satisfazem o gato com muito
                            menos quantidade. Sirva de pouco em pouco, pois a ração perde o cheiro rápido e eles gostam
                            sempre de ração “nova”.</p>

                        <p>O gato deverá ser criado <strong>DENTRO DE CASA</strong>! Se ele sair na rua poderá ser
                            roubado,
                            envenenado, se envolver em brigas, pegar doenças, ser maltratado ou atropelado. A
                            casa/apartamento deverá ter redes de proteção em todas as aberturas e a porta deverá ficar
                            sempre fechada, mesmo no verão. O gato castrado se torna mais caseiro e com comportamento
                            mais calmo, mas a qualquer descuido ele poderá escapar. Gato é curioso. Se você ama seu
                            gatinho, não deixe ele sair na rua. Ao contrário do dito popular: <strong>Gato não tem 7
                                vidas</strong>!</p>

                        <p>O gato persa é tradicionalmente conhecido pelo seu temperamento dócil e tranquilo, com
                            capacidade de adaptar-se a mudanças ambientais. Mia pouco e baixo, quando comparado as
                            demais raças. Muito carinhoso e extremamente limpo, "foge" de locais sujos e seus hábitos
                            diários são basicamente a alimentação, "toilette", diversão e sesta. Brincalhões, encontram
                            diversão nos objetos mais simples, e até nas próprias sombras. Os Persas adoram carícias,
                            cafunés e todas as expressões de carinho, e raramente ficam bravos. Os exóticos também são
                            muito queridos e carinhosos e são mais agitados. Eles brincam mais, correm mais, tem mais
                            energia que o persa.</p>

                        <h2 class="titulo-texto">Kit básico que você irá precisar:</h2>

                        <ol>
                            <li>
                                <p>Liteira (“banheiro” para areia sanitária).</p>
                            </li>

                            <li>
                                <p>Areia sanitária para gatos.</p>
                            </li>

                            <li>
                                <p>Ração Super Premium (não pode querer economizar nisso!).</p>
                            </li>

                            <li>
                                <p>Pá coletora.</p>
                            </li>

                            <li>
                                <p>Potinhos para ração e água, preferencialmente de inox ou cerâmica.</p>
                            </li>

                            <li>
                                <p>Rasqueadeira e pente de metal.</p>
                            </li>

                            <li>
                                <p>Arranhadores.</p>
                            </li>

                            <li>
                                <p>Bolinhas, ratinhos etc.</p>
                            </li>

                            <li>
                                <p>Vermífugo a cada 4 meses (eu uso o Drontal).</p>
                            </li>

                            <li>
                                <p>Gaze para limpar os olhos todos os dias.</p>
                            </li>

                            <li>
                                <p>Caixa de transporte.</p>
                            </li>
                        </ol>

                        <p>Todos os gatos devem ser vacinados. A 1ª vacina ocorre aos 2 meses, a 2ª aos 3 meses e a
                            vacina da raiva poderá ser feita a partir dos 4 meses. Todos os anos é preciso refazer 2
                            vacinas, uma dose de cada.</p>

                        <table class="ShekCrowley" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <iframe width="350" height="196" style="padding: 5px;"
                                            src="https://www.youtube.com/embed/y0JW9YySvW0" frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen>
                                        </iframe>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <p>Os filhotes devem tomar remédio para vermes 1 vez ao mês, até os 6 meses, sempre repetindo a
                            dose 15 dias depois, observando a quantidade de remédio em relação ao peso do gato. Após os
                            6 meses de vida passam a tomar a cada 3 ou 4 meses. Leia a bula, pois cada marca tem sua
                            dose correta.</p>

                        <table class="ShekCrowley" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <iframe width="350" height="196" style="padding: 5px;"
                                            src="https://www.youtube.com/embed/oeYlpBZAbNc" frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen>
                                        </iframe>

                                        <iframe width="350" height="196" style="padding: 5px;"
                                            src="https://www.youtube.com/embed/pzf7Km4vnGg" frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen>
                                        </iframe>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <p>O gato precisa de uma liteira ou bacia com areia higiênica para gato. É ótimo misturar um
                            saco de farinha de mandioca junto com um saco da areia. O xixi fica mais firme e o cheiro
                            diminui. Deixe a liteira num local que não caia água, que o gato tenha acesso a todo o
                            momento e que possa sujar, pois sempre cairão algumas pedrinhas no chão. E não deixe muito
                            próxima a ração do gato.</p>

                        <p>Os gatos precisam arranhar e para não arranharem seus móveis/sofá é preciso comprar um
                            ARRANHADOR para gatos. Quanto mais alto, mais eles vão gostar. Ensine ele a usar, insista.
                            Sempre que ele tentar arranhar o sofá, fale com ele com a voz firme e mostre que está
                            errado, mas nunca bata no gato. Fale: Não, não pode! E leve ele até o arranhador e esfregue
                            as patinhas dele no “poste” e as suas mãos também.</p>

                        <p>Os gatos persas têm os pelos muito compridos e finos. Muitos não gostam de ser escovados, mas
                            é necessário ESCOVAR no mínimo 3 vezes por semana. Se puder escovar dia sim, dia não, será
                            melhor. Assim, eles "acostumam" e reclamam menos, pois não criará nó e consequentemente não
                            irá puxar os pelos. Se você não escovar, criará nós enormes que só sairão tosando. E, além
                            disso, os gatos se lambem muito e vão engolir os pelos soltos e vão começar a VOMITAR. Além
                            da formação da "bola de pelos" no estomago, a ingestão excessiva de pelos ao se lamberem
                            provoca gastrite crônica, obstrução do intestino, úlcera e até câncer. </p>

                        <p>Com o tempo os pelos ficam oleosos, mas um banho resolve o problema. Os gatos que vivem
                            dentro de casa podem tomar BANHO 1 vez por mês ou a cada 2 ou 3 meses num pet shop de
                            confiança e com profissionais delicados. Certifique-se de que o Pet seja completamente
                            fechada com telas, redes de proteção para que o gato não consiga fugir (já aconteceram casos
                            de bichos fugirem e até morrerem em Pet que dão calmantes).

                        <p><strong>
                                <font color="red">IMPORTANTE:</font>
                            </strong>
                            pergunte se dão algum medicamento calmante/sedativo para o gato. Infelizmente existem
                            "profissionais" que não sabem dar banho em gatos acordados e dão sedativos para os gatos
                            ficarem mais calmos. Muitos gatos já morreram assim. Se usam algum medicamento calmante,
                            leve em outro pet shop!</p>
                        </p>

                        <p>É interessante manter as unhas aparadas com tesoura própria para gatos ou cortador de unhas
                            humano. Cortar somente a pontinha mais clara da unha porque tem veias que se cortar irá
                            sangrar muito. Repetindo: cortar somente a pontinha.</p>

                        <p>Use gaze umedecida para limpar os olhos uma vez por dia ou conforme a necessidade. Seque com
                            um pedaço de papel higiênico ou guardanapo. O lacrimejamento é normal na raça persa e
                            exótico por causa da carinha achatada. Não aperte em cima dos olhos, apenas passe a gaze
                            várias vezes até limpar.</p>

                        <p>No ouvido usar algodão seco ou umedecido com produto próprio para limpeza dos ouvidos 1 vez
                            por semana. Se houver secreção excessiva ou se o gato coçar muito as orelhas, consulte seu
                            veterinário, ele pode estar com fungo ou otite. Quanto antes tratar, melhor! Jamais use
                            cotonete, pois poderá ferir o ouvido do gato.</p>

                        <p>Sempre que levar para o banho, peça a <strong>TOSA HIGIÊNICA</strong> e, se desejar, a tosa
                            na
                            barriguinha toda. Dará menos trabalho para escovar. A tosa higiênica é extremamente
                            necessária para não grudar nos pelos quando forem no banheiro. </p>

                        <h2 class="titulo-texto">Produtos que eu uso e recomendo</h2>

                        <p><strong>Alimentação</strong>: sempre use ração SUPER PREMIUM. Isso é extremamente importante
                            para a
                            saúde do gato. Muda conforme a idade do gato (filhotes ou adultos). O gato é considerado
                            adulto a partir de 1 ano de idade.</p>

                        <p>Marcas Super Premium: Premier, Royal Canin, <strong>N&D da Farmina</strong>, Proplan, Hills.
                        </p>

                        <figure>
                            <img class="escala2" src="images/produtos/racao14.jpg" title="Premier" />
                            <img class="escala2" src="images/produtos/racao13.jpg" title="Royal Canin" />
                            <img class="escala2" src="images/produtos/racao16.jpg" title="Royal Canin" />
                            <img class="escala2" src="images/produtos/racao11.jpg" title="N&D da Farmina" />
                            <img class="escala2" src="images/produtos/guabi.jpg" title="Guabi Natural" />
                        </figure>

                        <p><strong>Não esqueça</strong>: é a ração que vai determinar a saúde do seu gatinho!</p>

                        <table class="ShekCrowley" width="100%">
                            <tbody>
                                <tr>
                                    <td>
                                        <iframe width="350" height="196" style="padding: 5px;"
                                            src="https://www.youtube.com/embed/vZFRqHe9ATA" frameborder="0"
                                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen>
                                        </iframe>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <p>Além da ração nossos gatos comem carne moída crua, frango cozido somente na água (sim, gatos
                            são carnívoros), atum em lata ao natural (sem óleo e sem tempero) e sachês específicos para
                            gatos.</p>

                        <p><strong>Areia</strong>: Areia: eu prefiro quando as pedrinhas são pequeninas, pois rende mais
                            e é mais
                            leve para mexer. Eu indico: Pipi Cat Classic grãos mais finos (nova), Progato Multigrãos,
                            Progato Premium e Progato Super Premium. </p>
                        <p> Sempre que os torrões de xixi desmontarem com facilidade misture farinha de mandioca na
                            areia. Os torrões não desmontando a areia não fica com cheiro.</p>

                        <figure>
                            <img class="escala2" src="images/produtos/pipicat.png" title="Pipi Cat Classic" />
                            <img class="escala2" src="images/produtos/progato_multi.png" title="Progato Multigrãos" />
                            <img class="escala2" src="images/produtos/progato_premium.png" title="Progato Premium" />
                            <img class="escala2" src="images/produtos/progato_super_premium.png"
                                title="Progato Super Premium" />
                            <img class="escala2" src="images/produtos/farinha.jpg" title="Farinha de Mandioca" />
                        </figure>

                        <p><strong>Liteira</strong> (Bacia): é bom que a liteira seja grande e que tenha as laterais
                            altas para
                            não cair tantas pedrinhas para fora. Mas pode ser qualquer bacia grande que tenha espaço
                            suficiente para caber o gato inteiro dentro e ele conseguir dar um giro completo dentro da
                            bacia. </p>

                        <figure>
                            <img class="escala2" src="images/produtos/liteira.jpg" title="Liteira" />
                            <img class="escala2" src="images/produtos/pazinha.jpg" title="Pá coletora" />
                        </figure>

                        <p><strong>Desinfetante</strong>: Herbalvet que possui ação bactericida, fungicida e viricida,
                            não é
                            corrosivo,
                            não mancha tecidos e nem os móveis. </p>
                        <p>Se o gato não está com problemas de fungos ou verminoses, poderá ser utilizado qualquer
                            produto de limpeza,
                            desde que seja suave e que não tenha cheiro forte.
                        </p>

                        <figure>
                            <img class="escala2" src="images/produtos/herbalvet.jpg" title="Herbvalvet" />
                        </figure>

                        <p><strong>Arranhador</strong>: o “poste” do arranhador deve ser alto para que o gato possa
                            arranhar
                            espichado, em pé (em torno de 50 cm). Lembre-se que eles precisam arranhar. Eu tenho vários
                            arranhadores. Acho bom ter eles espalhados pela casa. Se você puder comprar um bem alto seu
                            gato vai amar.</p>

                        <figure>
                            <img class="escala2" src="images/produtos/arranhador2.jpg" title="Arranhador" />
                            <img class="escala2" src="images/produtos/arranhador1.jpg" title="Arranhador" />
                            <img class="escala2" src="images/produtos/arranhador.jpg" title="Arranhador" />
                            <img class="escala2" src="images/produtos/brinquedos.jpg" title="Brinquedos" />
                        </figure>

                        <p><strong>Escova/pente</strong>: além da rasqueadeira, também é importante ter um pente de
                            metal. As
                            rasqueadeiras (aquelas de metal, com os dentes bem fininhos) devem ser usadas com
                            delicadeza, porque elas arranham a pele do gato. Elas são boas para remover os pelos mortos.
                            O pente de metal é ótimo para pentear no dia a dia e principalmente para abrir os nós.</p>

                        <figure>
                            <img class="escala2" src="images/produtos/pente.jpg" title="Pente" />
                            <img class="escala2" src="images/produtos/rasqueadeira.jpg" title="Rasqueadeira" /></p>
                        </figure>

                        <p><strong>Vermífugo</strong>: Sempre diga no pet shop que quer vermífugos para gatos. Leia a
                            bula para
                            saber a quantidade em relação ao peso do gato. Se você der demais poderá intoxicar o gato.
                            Eu uso o <strong>Drontal</strong>.</p>

                        <figure>
                            <img class="escala2" src="images/produtos/vermifugo.jpg" title="Vermífugo" />
                        </figure>

                        <p><strong>Caixa de transporte</strong>: Ao adquirir uma caixa de transporte tenha sempre em
                            mente que o
                            gatinho irá crescer. Por isso é bom comprar uma caixa um pouco maior (caixa Nº2), pois
                            assim, o gato não ficará apertado ao transportá-lo.</p>

                        <figure>
                            <img class="escala2" src="images/produtos/transporte.jpg" title="Caixa de transporte" />
                        </figure>

                        <p><strong>Veterinários</strong>: essa é uma decisão bastante séria, tem que ser alguém muito
                            confiável!!!
                            Alguns veterinários não são muito delicados, outros não atendem com muita atenção e ainda
                            tem os que não entendem bem sobre gatos. Alguns veterinários pensam que gatos são cachorros
                            pequenos. Gatos são diferentes de cachorros! Portanto, tenha muito cuidado ao escolher o
                            veterinário do seu gatinho. Se tiver algum especialista em felinos em sua cidade ou cidades
                            vizinhas, escolha esse profissional.</p>

                        <p>Como eu moro no RS, por isso só posso indicar veterinários por aqui:</p>

                        <ul style="margin:30px;">
                            <li>
                                <p><strong>Janice Thiessen</strong>, Clínica Espaço Animal, em Bento Gonçalves.
                                    <strong>(54) 3451.2428</strong> (quem faz a castração dos meus gatinhos). Ela é
                                    ótima!!!
                                </p>
                            </li>
                            <li>
                                <p><strong>Rafael (51) 9995.9027</strong> (quem faz as vacinas nos meus gatos adultos e
                                    filhotes e também cesariana quando necessário). Meus gatos nunca tiveram reação
                                    fazendo vacinas com ele. Ele me atende em casa.</strong>
                            </li>
                            <li>
                                <p><strong>Hospital Veterinário</strong>, em Novo Hamburgo. <strong>(51)
                                        3594.3172</strong>. Em casos mais sérios ou de emergência é o local mais
                                    confiável e acessível aqui na região. Tem vários veterinários e muitos aparelhos.
                                </p>
                            </li>
                            <li>
                                <p><strong>Ângela</strong> (oftalmologista), em Novo Hamburgo. Atende no Hospital
                                    Veterinário de NH. Ela é ótima.</p>
                            </li>
                        </ul>

                        <p id="pkd"><a href="#" class="scroll">Clique aqui para retornar ao início.</a></p>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>