<!DOCTYPE html>
<html>

<head>
    <title>Mamães e papais dos filhotes do Gatil Hauser - Gatil Hauser</title>
    <meta name="description"
        content="Mamães e papais dos filhotes do Gatil Hauser. Matrizes e padreadores de gatos persas e exóticos." />
    <meta name="keywords" content="gatos persas e exóticos, matrizes, padreadores, gatil, Gatil Hauser" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="images/icones/favicon.png" />

    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    
    <script src="js/jquery.chocolat.js"></script>
    <link rel="stylesheet" href="css/chocolat.css" type="text/css" media="screen" charset="utf-8">
    
    <script type="text/javascript" charset="utf-8">
        $(function() {
            $('.gallery-top a').Chocolat();
        });
    </script>
    <script src="js/menu_jquery.js"></script>
</head>

<body>
    <div class="header">
        <?php include "menu.php"; ?>
    </div>

    <div class="banner-head">
        <div class="banner-1"> </div>
        <div class="container">
            <h1>Mamães e papais</h1>
        </div>
    </div>

    <div class="gallery">
        <div class="container">
            <h2 class="titulo-texto" style="padding: 40px 0 15px 15px;">Mel</h2>

            <div style="padding: 0 0 10px 15px;">
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Nome: Hauser Mel</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Raça: Exótico</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Red Classic Tabby and White</p>
            </div>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_33.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_33.jpg" alt="Mel" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_34.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_34.jpg" alt="Mel" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_35.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_35.jpg" alt="Mel" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

            <h2 class="titulo-texto" style="padding: 40px 0 15px 15px;">Joey</h2>

            <div style="padding: 0 0 10px 15px;">
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Nome: Felice Cattus Lux Joey</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Raça: Exótico</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Cream Classic Tabby and White</p>
            </div>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_1.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_1.jpg" alt="Joey" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_4.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_4.jpg" alt="Joey" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_5.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_5.jpg" alt="Joey" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

            <h2 class="titulo-texto" style="padding: 40px 0 15px 15px;">Nina</h2>

            <div style="padding: 0 0 10px 15px;">
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Nome: Felice Cattus Lux Nina</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Raça: Exótico</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Blue Tortie and White</p>
            </div>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_40.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_40.jpg" alt="Nina" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_41.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_41.jpg" alt="Nina" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_42.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_42.jpg" alt="Nina" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

            <h2 class="titulo-texto" style="padding: 40px 0 15px 15px;">Zoey</h2>

            <div style="padding: 0 0 10px 15px;">
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Nome: Felice Cattus Lux Zoey</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Raça: Exótica</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Bicolor Blue</p>
            </div>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_9.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_9.jpg" alt="Zoey" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_21.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_21.jpg" alt="Zoey" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/exotico_12.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/exotico_12.jpg" alt="Zoey" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

            <h2 class="titulo-texto" style="padding: 40px 0 15px 15px;">Yumi</h2>

            <div style="padding: 0 0 10px 15px;">
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Nome: Hauser Yumi</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Raça: Persa</p>
                <p style="font-weight: bolder;font-size: medium;margin: 0;">Blue Tortie and White</p>
            </div>

            <div class="grid">
                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/persa_1.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/persa_1.jpg" alt="Zoey" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/persa_2.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/persa_2.jpg" alt="Zoey" />
                        </figure>
                    </a>
                </div>

                <div class="col-md-4 gallery-top">
                    <a href="images/mamaes_papais/persa_3.jpg" rel="title"
                        class="b-link-stripe b-animate-go  thickbox">
                        <figure class="effect-oscar">
                            <img src="images/mamaes_papais/persa_3.jpg" alt="Zoey" />
                        </figure>
                    </a>
                </div>

                <div class="clearfix"> </div>
            </div>

        </div>
    </div>
</body>

</html>